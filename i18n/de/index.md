[Kovri](https://getmonero.org/resources/moneropedia/kovri.html) ist eine freie, dezentrale Anonymitätstechnologie entwickelt von [Monero](https://getmonero.org).

Kovri basiert momentan auf den offenen Spezifikationen von [I2P](https://getmonero.org/resources/moneropedia/i2p.html) und nutzt sowohl [Garlic-Verschlüsselung](https://getmonero.org/resources/moneropedia/garlic-encryption.html) als auch [Garlic-Routing](https://getmonero.org/resources/moneropedia/garlic-routing.html), um ein privates, geschütztes Overlay-Netz über das Internet aufzubauen. Dieses Overlay-Netz ermöglicht es Benutzern, *wirksam* ihre geografische Lage und Internet-IP-Adresse zu verbergen.

Im Wesentlichen *ummantelt* Kovri den Internetverkehr einer Anwendung mit dem Ziel der Anonymität innerhalb des Netzwerks.

Als schlanker und sicherheitsorientierter Router ist Kovri vollständig mit dem I2P-Netzwerk kompatibel.

Beobachte die Entwicklung über das [Kovri-Repo](https://gitlab.com/kovri-project/kovri#downloads) und [werde Teil der Community](https://gitlab.com/kovri-project/kovri#contact).
