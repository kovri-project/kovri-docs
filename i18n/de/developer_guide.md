# Richtlinien
- Wir streben die vollständige Einhaltung von C++11/14 an; bitte nutze das zu deinem Vorteil
- Nutze bitte die Standardbibliothek und die Abhängigkeitsbibliotheken, wann immer es möglich ist

## Vulnerability Response
- Unser [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) ermutigt zur verantwortungsvollen Offenlegung
- Wir sind auch via [HackerOne](https://hackerone.com/monero) erreichbar

## Stil
1. Lies dir [Googles C++-Stilhandbuch](https://google.github.io/styleguide/cppguide.html) durch (insbesondere für nicht formatierende Stilverweise)
    - Bei Bash-Programmierung, lies [Googles Shell-Stilhandbuch](https://github.com/google/styleguide/blob/gh-pages/shell.xml)
2. Bei Dateien, die nur Neues enthalten, lasse [clang-format](http://clang.llvm.org/docs/ClangFormat.html) mit ```-style=file``` (das unser bereitgestelltes [.clang-format](https://gitlab.com/kovri-project/kovri/blob/master/.clang-format) nutzt) laufen
```bash
$ cd kovri/ && clang-format -i -style=file Quelle/Pfad/zu/meiner/Datei
```
3. Bei Dateien mit gemischter Arbeit (vorhandener + neuer), lasse [clang-format](http://clang.llvm.org/docs/ClangFormat.html) punktuell nur über die Zeilen laufen, die direkt mit der neuen Arbeit zusammenhängen.
   - In der [vim](http://clang.llvm.org/docs/ClangFormat.html#vim-integration)- und [emacs](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration)-Dokumentation finden sich Beispiele für die Einrichtung von Tastaturkürzeln für `clang-format`-Plugins.
4. Führe [cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint) aus (das unser bereitgestelltes [CPPLINT.cfg](https://gitlab.com/kovri-project/kovri/blob/master/CPPLINT.cfg) nutzt), um alle Probleme zu erfassen, die von clang-format nicht erkannt wurden
```bash
$ cd kovri/ && cpplint Quelle/Pfad/zu/meiner/Datei && [bearbeite die Datei manuell, um Fehlerbehebungen durchzuführen]
```

### Plugins

- Vim-Integration
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#vim-integration)
  - [clang-format ubuntu 16.04 vim Workaround](http://stackoverflow.com/questions/39490082/clang-format-not-working-under-gvim)
  - [cpplint.vim](https://github.com/vim-syntastic/syntastic/blob/master/syntax_checkers/cpp/cpplint.vim)
- Emacs-Integration
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) + [clang-format.el](https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.el)
  - [flycheck-google-cpplint.el](https://github.com/flycheck/flycheck-google-cpplint)

### Ergänzungen zu Googles vorgeschlagenem C++-Stil

- Vermeide vorangestellte gemischt groß- und kleingeschriebene (*mixed-case*) ```k``` und MACRO_TYPE für alle Konstanten
- Verwende Doxygens Drei-Slashes-```/// C++-Kommentare```, wenn du für Doxygen dokumentierst
- Versuche all deine Arbeit für Doxygen zu dokumentieren, während du Fortschritte machst
- Falls Anonymität ein Anliegen ist, versuche, dich dem Stil eines derzeitigen Mitwirkenden anzugleichen

### Optionale Checks
1. [cppdep](https://github.com/rakhimov/cppdep)
   für Komponentenabhängigkeit, physische Isolierung und include-Checks.
2. [cppcheck](https://github.com/danmar/cppcheck/) zur statischen Analyse
   (ergänzend zu Coverity).
3. [lizard](https://github.com/terryyin/lizard) für Code-Komplexitäts-Checks.

## Deine Arbeit abschicken
Um deine Arbeit beizusteuern, gehe bitte wie folgt vor:

1. [Forke](https://help.github.com/articles/fork-a-repo/) Kovri
2. Gehe nochmals den Stil-Abschnitt dieses Dokuments durch
3. Erstelle einen [themenspezifischen Branch](https://git-scm.com/book/de/v1/Git-Branching-Einfaches-Branching-und-Merging)
   - Wir haben momentan keine Tags, da da wir uns noch im Alpha-Stadium befinden. Du kannst deine Arbeit fürs Erste auf dem master-Branch aufbauen
4. Nimm Änderungen vor
   - Commits sollten wann immer möglich [atomar (*atomic*)](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) und Diffs einfach zu lesen sein
   - Versuche bitte, keine Ausbesserungen der Formatierung mit Nicht-Formatierungs-Commits zu vermischen
5. Sei höflich zum Git-Log
   - Commit-Titel sollten die Klasse oder den Aspekt des jeweiligen Projekts voranstellen. Zum Beispiel: "HTTPProxy: implement User-Agent scrubber. Fixes #193." oder "Garlic: fix uninitialized padding in ElGamalBlock"
   - Commit-Nachrichten sollten standardmäßig ausführlich sein und eine kurze Betreffzeile (höchstens 50 Zeichen), eine Leerzeile sowie einen detaillierten Erklärungstext mit getrennten Absätzen enthalten - es sei denn, der Titel alleine ist bereits selbsterklärend
   - Wenn ein bestimmter Commit auf ein anderes Issue verweist, füge bitte eine Referenz hinzu. Zum Beispiel: *See #123* oder *Fixes #123*. Das wird uns dabei helfen, Issues aufzulösen, wenn wir in den `master` mergen
   - Falls ein bestimmter Commit nach Zusammenarbeit innerhalb eines Pull-Requests einen Rebase erfährt, referenziere bitte die Pull-Request-Nummer in der Commit-Nachricht. Zum Beispiel: *References #123*
6. [**Signiere**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) deine(n) Commit(s) und, falls du ein neuer Mitwirkender bist, eröffne einen neuen Pull-Request, der deinen PGP-Schlüssel zu unserem Repository (siehe contrib) hinzufügt
7. Schicke einen Pull-Request an den Branch `master`
   - Der Hauptteil des Pull-Requests sollte eine genaue Beschreibung dessen enthalten, was der Patch macht, und (gegebenenfalls) eine Rechtfertigung/Begründung des Patches liefern. Du solltest Verweise auf jegliche Diskussionen, wie etwa andere Issues oder Chats auf IRC, beifügen

## Vorschläge
Um einen Vorschlag einzubringen, sieh dir bitte zunächst unsere [offenen Issues](https://gitlab.com/kovri-project/kovri/issues) nach bestehenden Vorschlägen durch. Falls das, was du vorschlägst, nicht enthalten ist, [öffne ein neues Issue](https://gitlab.com/kovri-project/kovri/issues/new).

Bitten wir darum, dass du aus den folgenden Gründen einen Vorschlag eröffnest:

1. Ein Vorschlag regt Kommunikation an
2. Ein Vorschlag zeigt, dass der Beitragende die Diskussionsbeiträge aller Projektmitwirkenden respektiert
3. Ein Vorschlag ermöglicht, dass Mitwirkende nahtlos ihre Diskussionsbeiträge in einem offenen Forum äußern können
4. Ein Vorschlag spart Zeit, falls ein Mitwirkender gerade an einem ähnlichen Feature/Issue arbeitet
5. Ein Vorschlag verhindert Katastrophen und Missgeschicke bzw. erlaubt es den Mitwirkenden, sich auf Katastrophen und Missgeschicke vorzubereiten

Einen Vorschlag *nicht* einzureichen, wird dich *nicht* daran hindern, etwas beizutragen. Wir werden deinen PR mergen - ein Vorschlag wird dennoch nachdrücklich empfohlen.

## TODOs
- Führe in der Codebase eine Schnellsuche nach ```TODO(unassigned):``` durch und/oder wähle ein Issue und beginne mit dem Patchen!
- Falls du ein TODO erstellst, weise es dir selbst zu oder schreibe es in ```TODO(unassigned):```

## Unit-test writing

Test writing is a well-trodden path whose process should not come as a surprise (as there are many decades of tests to study in the software repertoire). For this project, we will focus on the following when writing unit-tests as they are considered a standard *good practice*:

- Err on the side of [TDD](https://en.wikipedia.org/wiki/Test-driven_development) (refactor when necessary)
- Focus on modular programming / separation of concerns
- Test the quality of code coverage, not simply quantity
- Avoid running the same code paths across multiple tests
- Avoid copypasting implementation into test code

Also note that the state of the data - *not the context of the state* - should be held paramount as a driver for unit TDD.

Now, while there are many good, working examples of how to write unit-tests, let's look at some popular and recommended idioms as presented by our cousin [Tor](https://gitweb.torproject.org/tor.git/tree/doc/HACKING/WritingTests.md):

>If your code is very-low level, and its behavior is easily described in
terms of a relation between inputs and outputs, or a set of state
transitions, then it's a natural fit for unit tests.  (If not, please
consider refactoring it until most of it _is_ a good fit for unit
tests!)

>If your code adds new externally visible functionality to Tor, it would
be great to have a test for that functionality.  That's where
integration tests more usually come in.

>When writing tests, it's not enough to just generate coverage on all the
lines of the code that you're testing:  It's important to make sure that
the test _really tests_ the code.

>Remember, the purpose of a test is to succeed if the code does what
it's supposed to do, and fail otherwise.  Try to design your tests so
that they check for the code's intended and documented functionality
as much as possible.

>Often we want to test that a function works right, but the function to
be tested depends on other functions whose behavior is hard to observe, or
which require a working Tor network, or something like that.

>We talk above about "test coverage" -- making sure that your tests visit
every line of code, or every branch of code.  But visiting the code isn't
enough: we want to verify that it's correct.

>So when writing tests, try to make tests that should pass with any correct
implementation of the code, and that should fail if the code doesn't do what
it's supposed to do.

>You can write "black-box" tests or "glass-box" tests.  A black-box test is
one that you write without looking at the structure of the function.  A
glass-box one is one you implement while looking at how the function is
implemented.

>In either case, make sure to consider common cases *and* edge cases; success
cases and failure csaes.

>Tests shouldn't require a network connection.

>When possible, tests should not be over-fit to the implementation.  That is,
the test should verify that the documented behavior is implemented, but
should not break if other permissible behavior is later implemented.

In addition to not requiring a network connection, *unit-tests* should not require socket or filesystem access unless the test is socket/filesystem-specific test (these are unit-tests, not inteegration tests).

Other notes:

- Though we have a Docker testnet, we currenly lack any effective framework for integration and system testing. As such, the best we can do at the moment is effective unit testing.
- For gcov output when building tests, build with `make coverage`. This target should also build unit-tests.
- For existing kovri examples, see `crypto/{ed25519.cc,radix.cc}` and `util/buffer.cc` to name a few
- For effective unit-test writing outside of Tor, see Crypto++ and Monero unit-tests

## Fuzz-Testing

Aus der [Dokumentation](http://llvm.org/docs/LibFuzzer.html) : „LibFuzzer ist unter aktiver Entwicklung, man wird also die neueste (oder zumindest eine sehr aktuelle) Version des Clang-Kompilierers benötigen“

Hole dir eine aktuelle Clang-Version:

```bash
$ cd ~/ && mkdir TMP_CLANG && git clone https://chromium.googlesource.com/chromium/src/tools/clang TMP_CLANG/clang
$ ./TMP_CLANG/clang/scripts/update.py
$ cd --
```

Hole dir libFuzzer:

```bash
$ git clone https://chromium.googlesource.com/chromium/llvm-project/llvm/lib/Fuzzer contrib/Fuzzer
```

Erstelle Kovri mit eingeschaltetem Fuzz-Testing:

```bash
$ PATH="~/third_party/llvm-build/Release+Asserts/bin:$PATH" CC=clang CXX=clang++ make fuzz-tests
```

Nutzung (Beispiel für RouterInfo):

```bash
mkdir RI_CORPUS MIN_RI_CORPUS
find ~/.kovri/core/network_database/ -name "router_info*" -exec cp {} RI_CORPUS \;
./build/kovri-util fuzz --target=routerinfo -merge=1 MIN_RI_CORPUS RI_CORPUS
./build/kovri-util fuzz --target=routerinfo -jobs=2 -workers=2 MIN_RI_CORPUS
```

# Qualitätssicherung

Das Folgende ist ein Modellvorschlag für den QS-Arbeitsablauf. Obwohl der Ablauf grundsätzlich linear ist, kann jede Phase, falls nötig, einzeln bearbeitet werden - solange letztendlich alle Phasen behandelt werden.

## Phase 1: Grundlegende Überprüfung

- Gehe offene Issues über unseren [Issue-Tracker](https://gitlab.com/kovri-project/kovri/issues/) durch
- Sieh unseren [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) durch
- Der gesamte Code muss unseren Beitragsrichtlinien entsprechen
- Notiere Bereiche, die Verbesserung bedürfen (gedanklich oder im Code)
- Erstelle TODOs und weise sie, wenn möglich, zu

## Phase 2: Spezifikationsüberprüfung / Implementierung / Code-Dokumentation

- Komplettiere die Spezifikationsüberprüfung auf einer Pro-Modul-Basis, z.B.: Streaming, I2PControl, etc.
  - Der Code muss mit den wesentlichen Teilen der Spezifikation übereinstimmen, die dasselbe (oder ein besseres) Anonymitätsniveau wahren, als es Java-I2P bietet
  - Refaktoriere/implementiere/patche, wann/wo nötig
- Stelle eine C++11/14-konforme Implementierung sicher
  - Gehe bei Bedarf Phase 2 nochmals durch
- Erledige alle zugehörigen TODOs
- Dokumentiere Code so ausführlich wie möglich mit Inline-Kommentaren und Doxygen
  - Der Code sollte sowohl von Anfängern als auch erfahrenen Programmierern zu verstehen sein
  - Der Code sollte den Leser zu einem besseren Verständnis von I2P führen
    - I2P ist sehr komplex, daher sollte unser Code als eigenständiger Ersatz der Spezifikationsdokumentation fungieren und nicht nur einfach als eine Ergänzung (dies kann ein mühsames Ziel sein, ist aber sehr lohnend in Bezug auf Wartung und Software-Lebensdauer)

## Phase 3: Überprüfung der Kryptographie / Sicherheitsaudit

- Stelle sicher, dass die Kryptographie auf dem neuesten Stand und korrekt implementiert ist
- Ermittle jeden Vektor für bekannte Exploits
  - Beachte diese Vektoren beim Schreiben von Tests
- Attackiere Kovri auf jede mögliche Weise
  - Behebe das, was du kaputt gemacht hast
- Verwende, wenn möglich, immer vertrauenswürdige, gut geschriebene Bibliotheken
  - Vermeide Code der Sorte: *homebrewed*, ad-hoc, *Ich bin mir sicher, dass ich es besser als die Community weiß*
- Hole dir eine zweite (oder mehr) Meinung(en) von Kollegen ein, bevor du mit der nächsten Phase fortfährst

## Phase 4: Beseitigung von Bugs / Tests / Profiling

-  Löse Bugs/Issues mit Priorität
- Schreibe Unit-Tests für jedes Modul
  - Führe Tests durch. Führe sie nochmals durch
  - Vollständige Überprüfung der Testergebnisse. Patche, wenn nötig. Refaktoriere nach Bedarf
- Stelle sicher, dass die Automatisierung regelmäßig läuft
  - valgrind, doxygen, clang-format
  - Patche, wenn nötig. Refaktoriere nach Bedarf

## Phase 5: Rücksprache

- Halte Rücksprache mit Kollegen und der Community
  - Die Rücksprachen sollten öffentlich über Issues, Meetings und/oder IRC erfolgen
- Akzeptiere alle Rückmeldungen und produziere als Antwort greifbare Ergebnisse
- Wenn du zufrieden bist, gehe zur nächsten Phase über, andernfalls wiederhole diese Phase (oder beginne von einer vorherigen Phase aus)

## Phase 6: Wiederhole den Zyklus von Beginn an
