[Kovri](https://getmonero.org/resources/moneropedia/kovri.html) is een gratis, gedecentraliseerde technologie voor anonimiteit, ontwikkeld door [Monero](https://getmonero.org).

Kovri is momenteel gebaseerd op de open specificaties van [I2P](https://getmonero.org/resources/moneropedia/i2p.html) en maakt gebruik van zowel [garlic encryption](https://getmonero.org/resources/moneropedia/garlic-encryption.html) als [garlic routing](https://getmonero.org/resources/moneropedia/garlic-routing.html) om een beschermd privénetwerk bovenop het internet te creëren. Met dit overlaynetwerk kunnen gebruikers hun geografische locatie en hun IP-adres verbergen.

Kovri *verpakt* als het ware het internetverkeer van een applicatie, om het anoniem te maken op het netwerk.

Kovri vormt een op veiligheid gerichte compacte router en is volledig compatibel met het I2P-netwerk.

Je kunt volgen hoe de software wordt ontwikkeld in de [Kovri-repository](https://gitlab.com/kovri-project/kovri#downloads) en [deelnemen aan de community](https://gitlab.com/kovri-project/kovri#contact).
