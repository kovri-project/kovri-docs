# Monero via Kovri

## Aan de slag

1. Volg de instructies voor het compileren en/of installeren van [Kovri](https://gitlab.com/kovri-project/kovri) en [Monero](https://github.com/monero-project/monero).
2. Lees de [Gebruikershandleiding](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/nl/user_guide.md) in de taal waaraan je de voorkeur geeft.
3. Configureer server- en client-tunnels in Kovri om via het I2P-netwerk verbinding te maken met `monerod`-nodes.

## Kovri-servertunnel

Configureer eerst een servertunnel op één node.

Zie het bestand `tunnels.conf` in je gegevensdirectory voor het instellen van een servertunnel. Zie het bestand `kovri.conf` om je gegevensdirectory te vinden.

```
[XMR_P2P_Server]
type = server
address = 127.0.0.1
port = 28080
in_port = 28080
keys = xmr-p2p-keys.dat
;white_list =
;black_list =
```

Met deze configuratie zal Kovri luisteren op de standaard-P2P-poort van `monerod` voor het testnet. Lees vervolgens in de [Gebruikershandleiding](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/nl/user_guide.md) hoe je het adres van de bestemming in base32 vindt.

Als je al een Kovri-router hebt gestart, voer je `$ kill -HUP $(pgrep kovri)` uit om de nieuwe tunnel te laden. Of je voert een harde herstart uit door Kovri te stoppen en opnieuw te starten.

## Kovri-clienttunnel

Nu de servertunnel is ingesteld, beginnen we met het instellen van de client-tunnel door naar de servertunnel te verwijzen.

Voorbeeld:

```
[XMR_Client]
type = client
address = 127.0.0.1
port = 24040
dest = je-gekopieerde-serverbestemming.b32.i2p
dest_port = 28080
keys = xmr-client-keys.dat
```

Herhaal dit proces voor elke node waarvoor je verbinding wilt maken via Kovri.

Als je al een Kovri-router hebt gestart, voer je `$ kill -HUP $(pgrep kovri)` uit om de nieuwe tunnel te laden. Of je voert een harde herstart uit door Kovri te stoppen en opnieuw te starten.

## Monero P2P via Kovri

Het is net zo makkelijk om `monerod` te laten verwijzen naar je nieuwe client-tunnel in Kovri.

Hier is een voorbeeld voor het starten van een mining-node op het testnet:

```bash
monerod --testnet --start-mining adres-van-je-testnet-portemonnee --add-exclusive-node 127.0.0.1:24040
```

Als er verbindingsproblemen zijn, wacht dan tot je Kovri-nodes zijn opgenomen in het netwerk (ongeveer 5 tot 10 minuten na het starten).

## Monero RPC via Kovri

De RPC-service van Monero beschikbaar stellen via Kovri is een vergelijkbaar proces.

Configureer een servertunnel op je Kovri-node:

```
[XMR_RPC_Server]
type = server
address = 127.0.0.1
port = 28081
in_port = 28081
keys = xmr-rpc-keys.dat
;white_list =
;black_list =
```

Hierdoor worden een nieuwe set sleutels en een nieuw bestemmingsadres gegenereerd.

Als je hetzelfde bestemmingsadres als voor P2P wilt gebruiken, vervang je gewoon `xmr-rpc-keys.dat` door `xmr-p2p-keys.dat` in de bovenstaande configuratie.

Als je al een Kovri-router hebt gestart, voer je `$ kill -HUP $(pgrep kovri)` uit om de nieuwe tunnel te laden. Of je voert een harde herstart uit door Kovri te stoppen en opnieuw te starten.

Deze tunnel stelt de standaard JSON-RPC van het `monerod`-testnet beschikbaar. Je kunt de HTTP-client-tunnel van Kovri gebruiken om verbinding te maken.

Hier is een voorbeeld van verbinding maken met curl vanaf een Kovri-clientnode:

```bash
export http_proxy=127.0.0.1:4446 rpc_server=http://je-gekopieerde-rpc-bestemming.b32.i2p:28081
curl -X POST ${rpc_server}/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_height"}' -H 'Content-Type: application/json'
```
