# Richtlijnen
- We proberen volledig volgens C++11/14 te werken. Maak hier gebruik van.
- Gebruik zoveel mogelijk de standaardbibliotheek en bibliotheken van afhankelijkheden.

## Reactie op beveiligingsproblemen
- Ons [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) is bedoeld om verantwoordelijke bekendmaking te stimuleren.
- We zijn ook bereikbaar via [HackerOne](https://hackerone.com/monero).

## Stijl
1. Lees [de C++ Style Guide van Google](https://google.github.io/styleguide/cppguide.html) - vooral voor andere stijlkwesties dan opmaak (Formatting).
   - Lees [de Shell Style Guide van Google](https://github.com/google/styleguide/blob/gh-pages/shell.xml) als je bash-scripts programmeert.
2. Voer voor bestanden met alleen nieuw werk [clang-format](http://clang.llvm.org/docs/ClangFormat.html) uit met ```-style=file```. Daardoor wordt ons [.clang-format](https://gitlab.com/kovri-project/kovri/blob/master/.clang-format) gebruikt.
```bash
$ cd kovri/ && clang-format -i -style=file src/pad/naar/mijn/bestand
```
3. Voor bestanden met zowel nieuw als bestaand werk voert u [clang-format](http://clang.llvm.org/docs/ClangFormat.html) selectief uit voor alleen regels met nieuw werk.
   - Zie de documentatie van [vim](http://clang.llvm.org/docs/ClangFormat.html#vim-integration) en [emacs](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) voor voorbeelden van het configureren van sneltoetsen voor `clang-format`-plug-ins.
4. Voer [cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint) uit om problemen te vinden die niet zijn gevonden door clang-format. Hiervoor wordt onze [CPPLINT.cfg](https://gitlab.com/kovri-project/kovri/blob/master/CPPLINT.cfg) gebruikt.
```bash
$ cd kovri/ && cpplint src/pad/naar/mijn/bestand && (bewerk het bestand handmatig om problemen op te lossen)
```

### Plug-ins

- Vim-integratie:
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#vim-integration)
  - [Workaround voor clang-format in vim in Ubuntu 16.04](http://stackoverflow.com/questions/39490082/clang-format-not-working-under-gvim)
  - [cpplint.vim](https://github.com/vim-syntastic/syntastic/blob/master/syntax_checkers/cpp/cpplint.vim)
- Emacs-integratie:
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) + [clang-format.el](https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.el)
  - [flycheck-google-cpplint.el](https://github.com/flycheck/flycheck-google-cpplint)

### Afwijkingen van de door Google voorgestelde C++-stijl

- Vermijd het voorvoegen van een ```k``` en MACRO_TYPE voor alle constanten.
- Gebruik ```/// C++- commentaar``` met drie schuine strepen wanneer je documentatie voor Doxygen schrijft.
- Probeer tijdens het programmeren al je werk te documenteren voor Doxygen.
- Als je je zorgen maakt over anonimiteit, probeer je dan aan te passen aan de stijl van een andere programmeur.

### Optionele controles
1. [cppdep](https://github.com/rakhimov/cppdep)
   voor onderdeelafhankelijkheid, fysieke isolatie en include-controles
2. [cppcheck](https://github.com/danmar/cppcheck/) voor statische analyse
   (als aanvulling op Coverity)
3. [lizard](https://github.com/terryyin/lizard) om te controleren op de complexiteit van code

## Je werk insturen
Ga als volgt te werk om een bijdrage te leveren:

1. [Maak een vork](https://help.github.com/articles/fork-a-repo/) van Kovri.
2. Bestudeer het gedeelte over stijl in dit document.
3. Maak een [topic branch](https://git-scm.com/book/nl/v2/Branchen-in-Git-Eenvoudig-branchen-en-mergen).
   - We gebruiken momenteel geen tags omdat we in de alfa-fase zijn. Voorlopig kun je je werk baseren op de master-branch.
4. Breng wijzigingen aan.
   - Commits moeten [atomisch](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) zijn indien mogelijk en diffs moeten gemakkelijk te lezen zijn.
   - Combineer wijzigingen van opmaak niet met inhoudelijke wijzigingen.
5. Behandel de git-log met respect.
   - De titel van een commit moet beginnen met de klasse of het aspect van het project. Bijvoorbeeld: "HTTPProxy: implement User-Agent scrubber. Fixes #193." of "Garlic: fix uninitialized padding in ElGamalBlock".
   - Commitberichten moeten standaard informatief zijn, met een korte onderwerpregel (maximaal 50 tekens), een lege regel en een gedetailleerde uitleg, eventueel verdeeld in alinea's - tenzij de titel vanzelf spreekt. 
   - Voeg een verwijzing toe als een commit met een issue te maken heeft. Bijvoorbeeld *See #123* of *Fixes #123*. Op die manier kunnen we issues afsluiten bij het mergen in de `master`.
   - Als een bepaalde commit wordt gerebaset na samenwerking binnen een pull request, vermeld dan het nummer van dat pull request in het commitbericht. Bijvoorbeeld: *References #123*
6. [**Onderteken**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) je commits. En als je een nieuwe bijdrager bent: open een nieuw pull request om je PGP-sleutel toe te voegen aan onze repository.
7. Verzend een pull request naar de `master`-branch.
   - De hoofdtekst van het pull request moet een nauwkeurige beschrijving bevatten van wat de patch doet en (indien van toepassing) waarom de patch nodig is. Verwijs ook naar relevante discussies, zoals andere issues of chats op IRC.

## Voorstellen
Controleer of er al voorstellen bestaan in onze [openstaande issues](https://gitlab.com/kovri-project/kovri/issues) voordat je een voorstel doet. [Open een nieuw issue](https://gitlab.com/kovri-project/kovri/issues/new) als het voorstel dat je wilt doen er niet bij staat.

Hoewel onze gedragscode bepaalt dat we alles mergen, vragen we je om de volgende redenen een voorstel te doen:

1. Een voorstel opent communicatie.
2. Met een voorstel toont de bijdrager respect voor iedereen die meewerkt aan het project.
3. Een voorstel maakt naadloze input van samenwerkers in een open forum mogelijk.
4. Een voorstel bespaart tijd als iemand anders aan een zelfde soort functie of probleem werkt.
5. Een voorstel voorkomt ongelukken of helpt anderen zich voor te bereiden op ongelukken.

Het is *niet* onmogelijk om bij te dragen *zonder* een voorstel te doen - we zullen je pull request mergen - maar we raden je dringend aan een voorstel te doen.

## TODO's
- Zoek in de code naar ```TODO(unassigned):``` of kies een issue uit en maak een patch!
- Als je een TODO toevoegt, wijs die dan aan jezelf toe, of voer ```TODO(unassigned):``` in.

## Unit-test writing

Test writing is a well-trodden path whose process should not come as a surprise (as there are many decades of tests to study in the software repertoire). For this project, we will focus on the following when writing unit-tests as they are considered a standard *good practice*:

- Err on the side of [TDD](https://en.wikipedia.org/wiki/Test-driven_development) (refactor when necessary)
- Focus on modular programming / separation of concerns
- Test the quality of code coverage, not simply quantity
- Avoid running the same code paths across multiple tests
- Avoid copypasting implementation into test code

Also note that the state of the data - *not the context of the state* - should be held paramount as a driver for unit TDD.

Now, while there are many good, working examples of how to write unit-tests, let's look at some popular and recommended idioms as presented by our cousin [Tor](https://gitweb.torproject.org/tor.git/tree/doc/HACKING/WritingTests.md):

>If your code is very-low level, and its behavior is easily described in
terms of a relation between inputs and outputs, or a set of state
transitions, then it's a natural fit for unit tests.  (If not, please
consider refactoring it until most of it _is_ a good fit for unit
tests!)

>If your code adds new externally visible functionality to Tor, it would
be great to have a test for that functionality.  That's where
integration tests more usually come in.

>When writing tests, it's not enough to just generate coverage on all the
lines of the code that you're testing:  It's important to make sure that
the test _really tests_ the code.

>Remember, the purpose of a test is to succeed if the code does what
it's supposed to do, and fail otherwise.  Try to design your tests so
that they check for the code's intended and documented functionality
as much as possible.

>Often we want to test that a function works right, but the function to
be tested depends on other functions whose behavior is hard to observe, or
which require a working Tor network, or something like that.

>We talk above about "test coverage" -- making sure that your tests visit
every line of code, or every branch of code.  But visiting the code isn't
enough: we want to verify that it's correct.

>So when writing tests, try to make tests that should pass with any correct
implementation of the code, and that should fail if the code doesn't do what
it's supposed to do.

>You can write "black-box" tests or "glass-box" tests.  A black-box test is
one that you write without looking at the structure of the function.  A
glass-box one is one you implement while looking at how the function is
implemented.

>In either case, make sure to consider common cases *and* edge cases; success
cases and failure csaes.

>Tests shouldn't require a network connection.

>When possible, tests should not be over-fit to the implementation.  That is,
the test should verify that the documented behavior is implemented, but
should not break if other permissible behavior is later implemented.

In addition to not requiring a network connection, *unit-tests* should not require socket or filesystem access unless the test is socket/filesystem-specific test (these are unit-tests, not inteegration tests).

Other notes:

- Though we have a Docker testnet, we currenly lack any effective framework for integration and system testing. As such, the best we can do at the moment is effective unit testing.
- For gcov output when building tests, build with `make coverage`. This target should also build unit-tests.
- For existing kovri examples, see `crypto/{ed25519.cc,radix.cc}` and `util/buffer.cc` to name a few
- For effective unit-test writing outside of Tor, see Crypto++ and Monero unit-tests

## Fuzz-testen

Uit [de documentatie](http://llvm.org/docs/LibFuzzer.html): "LibFuzzer wordt actief ontwikkeld, dus u heeft de huidige (of tenminste een recente) versie van de compiler Clang nodig."

Haal een recente versie van clang op:

```bash
$ cd ~/ && mkdir TMP_CLANG && git clone https://chromium.googlesource.com/chromium/src/tools/clang TMP_CLANG/clang
$ ./TMP_CLANG/clang/scripts/update.py
$ cd --
```

Haal libFuzzer op:

```bash
$ git clone https://chromium.googlesource.com/chromium/llvm-project/llvm/lib/Fuzzer contrib/Fuzzer
```

Compileer kovri met fuzz-testen ingeschakeld:

```bash
$ PATH="~/third_party/llvm-build/Release+Asserts/bin:$PATH" CC=clang CXX=clang++ make fuzz-tests
```

Gebruik (voorbeeld voor RouterInfo):

```bash
mkdir RI_CORPUS MIN_RI_CORPUS
find ~/.kovri/core/network_database/ -name "router_info*" -exec cp {} RI_CORPUS \;
./build/kovri-util fuzz --target=routerinfo -merge=1 MIN_RI_CORPUS RI_CORPUS
./build/kovri-util fuzz --target=routerinfo -jobs=2 -workers=2 MIN_RI_CORPUS
```

# Kwaliteitscontrole (QA)

We stellen het volgende model voor QA-workflow voor. Hoewel het een lineair model is, kun je aan elke fase afzonderlijk werken, zolang alle fasen uiteindelijk maar aan bod komen.

## Fase 1: Basiscontrole

- Bekijk openstaande issues in onze [issuetracker](https://gitlab.com/kovri-project/kovri/issues/).
- Raadpleeg ons [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md).
- Alle code moet voldoen aan onze richtlijnen voor bijdragen.
- Noteer verbeterpunten (eventueel in de code).
- Maakt TODO's aan en wijs ze indien mogelijk toe.

## Fase 2: Specificatiecontrole/Implementatie/Documentatie

- Voer de specificatiecontrole uit per module, bijvoorbeeld Streaming, I2PControl enzovoort.
  - De code moet overeenkomen met essentiële onderdelen van de specificatie die hetzelfde niveau van anonimiteit bieden als Java I2P (of beter).
  - Zorg dat je de code waar nodig opschoont/implementeert/patcht.
- Zorg dat je implementatie de standaard C++11/14 naleeft.
  - Controleer eventueel fase 2.
- Los alle gerelateerde TODO's op.
- Documenteer de code zoveel mogelijk met commentaar en Doxygen.
  - De code moet begrijpelijk zijn voor beginnende en ervaren programmeurs.
  - De code moet de lezer helpen I2P beter te begrijpen.
    - I2P is erg complex, dus onze code moet dienen als zelfstandige vervanging van specificatiedocumentatie en niet slechts als een aanvulling. (Dit is soms vervelend werk, maar het wordt op termijn beloond op het gebied van onderhoud en levensduur van de software.)

## Fase 3: Controle van cryptografie en beveiliging

- Zorg dat de cryptografie up-to-date en juist geïmplementeerd is.
- Stel elke vector voor bekende aanvallen vast.
  - Houd rekening met deze aanvalsvectoren bij het programmeren van tests.
- Probeer Kovri op elke mogelijke manier te kraken.
  - Los de beveiligingsproblemen op die je vindt.
- Gebruik altijd degelijke, betrouwbare bibliotheken als die er zijn.
  - Vermijd eigenwijze code met zelf in elkaar geknutselde methoden.
- Vraag aan collega's advies voordat je verder gaat met de volgende fase.

## Fase 4: Bugs/Tests/Analyses

- Los de bugs/issues op die prioriteit hebben.
- Programmeer *unit tests* voor elke module. 
  - Voer de tests uit. En voer ze nog een keer uit.
  - Voer een volledige controle van de testresultaten uit. Voeg indien nodig patches toe. Herstructureer de code indien nodig.
- Zorg dat automatische processen regelmatig worden uitgevoerd.
  - valgrind, doxygen, clang-format.
  Voeg indien nodig patches toe. Herstructureer de code indien nodig.

## Fase 5: Overleg

- Overleg met collega's en de community.
  - Overleg moet in het openbaar plaatsvinden via issues, vergaderingen en/of IRC.
- Accepteer alle feedback en reageer met tastbare resultaten.
- Als je tevreden bent, ga dan verder met de volgende fase. Zo niet, herhaal dan deze fase (of begin bij een eerdere fase).

## Fase 6: Herhaal deze cyclus vanaf het begin.
