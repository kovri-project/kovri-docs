# Aan de slag met het Kovri-testnet

## Vooraf

Het Kovri-testnet bevindt zich momenteel binnen een serie Docker-containers en -images die allemaal over één Docker-netwerk communiceren.
Op deze manier kun je het netwerk testen en bewaken zonder verbinding te maken met het openbare Kovri-netwerk.

## Vereisten

- Linux-ontwikkelomgeving (Linux wordt momenteel ondersteund).
   - Zie de [README](https://gitlab.com/kovri-project/kovri/blob/master/i8n/nl/README.md#building) van Kovri voor een lijst met afhankelijkheden voor het compileren.
- [Docker](https://www.docker.com/).
   - De compilerende gebruiker moet toegangsrechten hebben om Docker te gebruiken (bijvoorbeeld toegevoegd zijn aan de Docker-groep).
- Een gekloonde repository.
```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
```

## Stap 1. Maak het testnet

Voer de optie `help` uit voor een volledige lijst met opties:
```bash
$ ./kovri/contrib/testnet/testnet.sh help
```
Je kunt de vermelde omgevingsvariabelen exporteren uit de shell of handmatig instellen tijdens het installeren.

Maak de omgeving aan en stel de waarden daarvoor in:
```bash
$ ./kovri/contrib/testnet/testnet.sh create
```
- Voor nieuwe ontwikkelaars raden we aan om de standaardwaarden te gebruiken, met uitzondering van de locatie van de repository.
- Zie de directory Dockerfiles voor de Dockerfiles die beschikbaar zijn tijdens het installeren.
- Selecteer een directory buiten de Kovri-repository, zodat de repository niet wordt vervuild.

## Stap 2a. Start het testnet

```bash
$ ./kovri/contrib/testnet/testnet.sh start
```
Voer `help` uit voor meer informatie over opties voor bewaken.

## Stap 2b. Voer aangepaste opdrachten uit

**TODO(unassigned): improve this section**

Zie de documentatie van Docker voor het uitvoeren van opdrachten vanuit een container.

Je kunt ook het volgende proberen:
```bash
$ ./contrib/testnet/testnet.sh exec "bash"
```

## Stap 3. Stop het testnet

```bash
$ ./kovri/contrib/testnet/testnet.sh stop
```
- Er wordt gevraagd een time-out-interval in te stellen voor de container (als de omgeving niet is ingesteld).
   - Dit is handig voor het geval dat een router blijft hangen.

## Stap 4. Verwijder het testnet

```bash
$ ./kovri/contrib/testnet/testnet.sh destroy
```
- Er wordt gevraagd of je de testnet-directory wilt wissen (als de omgeving niet is ingesteld).
