# Veelgestelde vragen (FAQ)

## Inhoud
- Algemene vragen
  - Wat is Kovri?
  - Wie ontwikkelt Kovri?
  - Wat draagt Kovri bij aan Monero?
  - Waarom zou ik Kovri gebruiken in plaats van I2P?
  - Wat doet Kovri wel/niet voor mij?
  - Wat is de huidige status van Kovri?
  - Wanneer wordt de alfa-versie uitgebracht?
  - Waar richten de ontwikkelaars zich momenteel op?
  - Hoe bruikbaar is Kovri op dit moment en hoeveel privacy biedt het?
  - Hoe kan ik contact opnemen met de Kovri-ontwikkelaars?
- Het verschil tussen Kovri en i2pd
  - Waarom zou ik Kovri gebruiken in plaats van i2pd?
  - Wat zijn de grootste verschillen tussen Kovri en i2pd?
  - Waarom hebben jullie een fork van i2pd gemaakt?
  - Wat waren de keerpunten waardoor jullie je hebben afgescheiden van i2pd (en waarom zijn er twee i2pd repositories: een op Bitbucket en een op GitHub)?
- Kovri gebruiken
  - Ik heb een beveiligingsprobleem gevonden! Ik heb een bug gevonden! Wat moet ik doen?
  - Waarom is de datum/tijd in mijn log anders dan mijn tijdzone?

## Algemene vragen

### Wat is Kovri?

[Kovri](https://getmonero.org/resources/moneropedia/kovri.html) is een gratis, gedecentraliseerde technologie voor anonimiteit, ontwikkeld door [Monero](https://getmonero.org).

Kovri is momenteel gebaseerd op de open specificaties van [I2P](https://getmonero.org/resources/moneropedia/i2p.html) en maakt gebruik van zowel [garlic encryption](https://getmonero.org/resources/moneropedia/garlic-encryption.html) als [garlic routing](https://getmonero.org/resources/moneropedia/garlic-routing.html) om een beschermd privénetwerk bovenop het internet te creëren. Met dit overlaynetwerk kunnen gebruikers hun geografische locatie en hun IP-adres verbergen.

Kovri *verpakt* als het ware het internetverkeer van een applicatie, om het anoniem te maken op het netwerk.

Kovri vormt een op veiligheid gerichte compacte router en is volledig compatibel met het I2P-netwerk. Er wordt gewerkt aan een alfa-versie van Kovri.

### Wie ontwikkelt Kovri?
Kovri is een open source-project dat afhankelijk is van vrijwillige bijdragen. De hoofdontwikkelaar van het project is [anonimal](https://github.com/anonimal). Je kunt vragen aan hem stellen via de IRC-kanalen [#kovri](irc://chat.freenode.net/#kovri) en [#kovri-dev](irc://chat.freenode.net/#kovri-dev) en zijn [Twitter-account](https://twitter.com/whoisanonimal).

Kovri wordt ontwikkeld als onderdeel van [het Monero-project](https://github.com/monero-project), een ander open source-project voor [de cryptovaluta Monero](https://getmonero.org) en [Open Alias](https://openalias.org). Het Monero-project en Kovri kunnen van elkaar profiteren: Kovri kan worden geïntegreerd in het Monero-netwerk, terwijl Monero ontwikkelaars en middelen ter beschikking stelt voor het ontwikkelen van Kovri.

### Wat draagt Kovri bij aan Monero?
Monero is een veilige, anonieme, niet-traceerbare en verwisselbare cryptovaluta met standaard privacy, dankzij technieken als verborgen adressen, vertrouwelijke transacties en ring-handtekeningen, waarmee respectievelijk de ontvanger, het bedrag en de afzender worden verborgen. Potentiële zwakke punten van Monero zijn het publiceren van het IP-adres waardoor een transactie wordt verzonden en correlatie-analyse.

Daarvoor werken we aan Kovri. Kovri zal in de officiële Monero-portemonnee worden toegepast, zodat alle transacties via het anonieme netwerk van Kovri worden verzonden en de IP-adressen waar ze vandaan komen worden verborgen. In de toekomst worden alle transacties standaard via Kovri verzonden, hoewel de blockchain nog steeds in het openbaar wordt gedownload, omdat dat efficiënter is.

### Waarom zou ik Kovri gebruiken in plaats van I2P?
[I2P](https://geti2p.net) is een fantastisch project, maar we hebben een paar dingen moeten aanpassen om de technologie te kunnen integreren in Monero. In de eerste plaats: I2P is in Java geprogrammeerd. Volgens ons zou een in C++ geprogrammeerde router sneller en lichter zijn.

In de tweede plaats: de Java-implementatie van I2P is prima, maar bevat veel extra functies die volgens ons niet nodig zijn voor Monero. Daarom hebben we besloten om opnieuw te beginnen en een router te maken die ALLEEN een router is. Deze minimalistische benadering is perfect voor Monero, maar ook handig voor anderen die I2P-applicaties willen maken. Ze krijgen de optie om een lichtgewicht-router zonder alle toeters en bellen te gebruiken, terwijl anderen die deze extra functies wel nodig hebben, de Java-implementatie kunnen gebruiken. Het is een win-winsituatie voor iedereen.

### Wat doet Kovri op dit moment?
- Je kunt een node op het I2P-netwerk worden.
- Je fysieke locatie wordt verborgen voor de sites die je bezoekt.
- Je bent anoniem op IRC en kunt anonieme e-mail gebruiken.
- Je kunt anonieme websites of services hosten.
- Ontwikkelaars, hackers en onderzoekers worden gefinancierd via het Monero-forum en HackerOne.
- Er worden strenge eisen gesteld aan de kwaliteit van code en programmeermethoden.

### Wat kunnen we in de toekomst verwachten van Kovri?
- Monero-transacties uitvoeren via I2P
- Grafische interface voor betere bruikbaarheid en configuratiemogelijkheden
- Bibliotheek-API en bindingen voor externe apps/bibliotheken
- Firefox-extensie om gemakkelijk eepsites te kunnen bezoeken
- Website-ontwikkeling ([kovri.io](https://kovri.io) / [kovri.i2p](http://kovri.i2p))
- Uitgebreide documentatie van beginners- tot ontwikkelaarsniveau

### Wat doet Kovri niet voor mij?
- Uw veiligheid opofferen voor verdachte bijbedoelingen
- Een lastige, ingewikkelde webinterface bieden
- Consensus van het netwerk baseren op autoriteiten
- Toegang bieden tot gewone websites via een *outproxy*
- Een trage Java VM vereisen
- Je hond uitlaten of je belasting betalen

### Wat is de huidige status van Kovri?
Kovri wordt actief ontwikkeld en bevindt zich momenteel in de alfa-fase. Het is *nog niet* geïntegreerd in Monero, maar naast bepaalde kernfuncties ontwikkelen we een [client-API](https://gitlab.com/kovri-project/kovri/issues/351) en een [kern-API](https://gitlab.com/kovri-project/kovri/issues/350) API die kunnen worden gebruikt voor Monero en andere applicaties.

Maar dat we in de alfa-fase zijn betekent niet dat je Kovri niet kunt gebruiken. Op dit moment kun je Kovri gebruiken om verbinden te maken met het I2P-netwerk (en het te gebruiken), om eepsites te bezoeken, verbinding te maken met IRC, en client- en servertunnels te draaien.

### Wanneer wordt de alfa-versie uitgebracht?
Een alfa-versie wordt hopelijk voor het einde van 2017 uitgebracht. Na de alfa-release beginnen we meteen te werken aan de bèta-release. Daarvoor is het volgende nodig: een volledig geïmplementeerde API, het oplossen van belangrijke kwaliteitsproblemen en het oplossen van verschillende bugs.

### Waar richten de ontwikkelaars zich momenteel op?
Momenteel richten we ons op alles wat wordt vermeld in onze [issuetracker](https://gitlab.com/kovri-project/kovri/issues/). Deze issues vormen de bulk van wat we moeten voltooien voor de officiële alfa-release.

### Hoe bruikbaar is Kovri op dit moment en hoeveel privacy biedt het?
De bruikbaarheid van Kovri beperkt zich tot de helptekst in `./kovri --help`. Kovri werkt op dit moment nog niet samen met Monero. Op het gebied van privacy hebben we sinds het begin veel beveiligingsproblemen opgelost, maar houd er rekening mee dat we nog steeds in de alfa-fase zijn.

Er valt nog steeds veel te doen, dus verwacht niet een sterke garantie van anonimiteit, zoals met Tor, of zelfs Java-I2P. Die projecten zijn het resultaat van meer dan tien jaar onderzoek en praktijkervaring, terwijl wij net komen kijken.

Experimenteer gerust met Kovri als een ontwikkelaar, maar alleen als **niet** anoniem zijn geen gevaar voor je oplevert, want er bestaat altijd het risico dat je je anonimiteit kwijtraakt met alfa-software. (Dit is niet uniek voor Kovri.)

### Hoe kan ik contact opnemen met de Kovri-ontwikkelaars?
Zie onze [README](https://gitlab.com/kovri-project/kovri/blob/master/i8n/nl/README.md).

## Het verschil tussen Kovri en i2pd

### Waarom zou ik Kovri gebruiken in plaats van i2pd?

- Veiligheid: onze prioriteit is het beveiligen van onze software, niet [kostte wat het kost zo snel mogelijk een release uitbrengen](https://gitlab.com/kovri-project/kovri/issues/65).
- Kwaliteit: je ondersteunt het streven om een codebase te ontwikkelen die de tand des tijds kan doorstaan. Dat betekent ook dat we de code onderhoudbaar willen maken.
- Monero: terwijl je je privacy en anonimiteit versterkt, ondersteun je ook een cryptovaluta die is gericht op het beschermen van je privacy en anonimiteit.

### Wat zijn de grootste verschillen tussen Kovri en i2pd?

- We hebben een [Forum Funding System](https://forum.getmonero.org/8/funding-required) om functies en code te betalen.
- We richten ons op het creëren van een I2P-router die [standaard veilig](http://www.openbsd.org/security.html) is, eenvoudig te onderhouden en beter te controleren. Dit gaat ten koste van minder gebruikte functies die wel in andere routers voorkomen. Maar de kernfuncties blijven aanwezig, inclusief API. Door een kleinere, efficiënte, minimalistische router te maken, geven we ontwikkelaars en onderzoekers meer tijd voor beveiligingscontrole en meer tijd om het ontwerp en de specificaties van I2P te analyseren.
- We richten ons op het implementeren van een intuïtieve, gebruiksvriendelijke API, waarmee elke applicatie (inclusief Monero) verbinding kan maken met het I2P-netwerk.
- We bieden zowel eindgebruikers als ontwikkelaars een model voor [kwaliteitsbewaking](https://gitlab.com/kovri-project/kovri/issues/58) en [ontwikkeling](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/contributing.md), zodat we voor iedereen betere software kunnen maken.
- We zullen alternatieve opties voor reseeding implementeren, zodat gebruikers daarvoor [Pluggable Transports](https://www.torproject.org/docs/pluggable-transports.html.en) kunnen gebruiken in plaats van HTTPS.
- We zullen uitgebreide functionaliteit invoeren *(verborgen modus + inkomend verkeer uitgeschakeld)* om anonimiteit te bieden aan mensen die in landen met extreme beperkingen wonen of geblokkeerd worden door een NAT of DS-Lite op ISP-niveau.
- We zullen altijd een gastvrije omgeving voor samenwerking bieden.
- We zullen altijd naar je feedback luisteren en ons best doen om Kovri te verbeteren.

### Waarom hebben jullie een fork van i2pd gemaakt?

We hebben ons om verschillende redenen afgesplitst:

- We wilden een robuuste, veilige en bruikbare implementatie van het I2P-netwerk in C++ en dat was niet wat i2pd ons bood.
- We wilden een positieve community die samenwerking om de software te verbeteren zou aanmoedigen - niet negatieve, narcistische eerzucht.
- We wilden een hoofdontwikkelaar die leiding zou geven - niet iemand die verzoeken om verantwoordelijke bekendmaking negeert of het hazepad kiest bij een conflict tussen samenwerkers.

### Wat waren de keerpunten waardoor jullie je hebben afgescheiden van i2pd (en waarom zijn er twee i2pd repositories: een op Bitbucket en een op GitHub)?

*Aldus begon het drama van i2pd*.

In 2015 pushte een van de ontwikkelaars met push-privileges op GitHub een commit waar de eerste auteur van i2pd het niet mee eens was. Deze auteur werkte niet samen om het conflict op te lossen, maar verhuisde i2pd naar Bitbucket, verwijderde de **complete** geschiedenis in git en maakte zichzelf de enige 'bijdrager' van de software. Vervolgens zwoer hij nooit meer terug te keren op Irc2P.

Veel leden van de I2P-community namen hier aanstoot aan, inclusief de ontwikkelaars. Hierdoor eindigde het C++-project bijna.

In het najaar van 2015 blies anonimal, die het werk van zo veel mensen niet verloren wilde zien gaan, het project nieuw leven in met bijdragen van zichzelf en anderen. Vervolgens werden alle resterende actieve ontwikkelaars uitgenodigd om de toekomst van i2pd te bespreken. De eerste auteur van i2pd liet niet van zich horen maar voelde zich blijkbaar beledigd. Hij [nam wraak](https://github.com/PurpleI2P/i2pd/issues/279) en begon weer op GitHub te werken, maar deze keer in de branch ```openssl``` (die gelijk bleek te zijn aan de Bitbucket-repository) in plaats van de ```master```-branch waar de community aan werkte.

Omdat zulk grillig gedrag alleen maar schadelijk zou zijn voor het I2P-netwerk en het project als geheel, hebben de overige ontwikkelaars nog [een aantal belangrijke vergaderingen](https://gitlab.com/kovri-project/kovri/issues/47) gehouden en de basis gelegd voor wat nu Kovri heet.

## Kovri gebruiken

### Ik heb een beveiligingsprobleem gevonden! Ik heb een bug gevonden! Wat moet ik doen?
- Beveiligingsproblemen: Zie onze [README](https://gitlab.com/kovri-project/kovri/blob/master/i8n/nl/README.md).
- Bugs: zie onze [Handleiding voor ontwikkelaars](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/nl/developer_guide.md).

### Waarom is de datum/tijd in mijn log anders dan mijn tijdzone?
Tijden worden in UTC gelogd om je privacy te beschermen. Doordat we UTC gebruiken, is het makkelijker om logs te delen met ontwikkelaars of de community zonder je privacy aan te tasten.
