[<img width="300" src="https://static.getmonero.org/images/kovri/logo.png" alt="Kovri" />](https://gitlab.com/kovri-project/kovri)

1. [Bedekken, verbergen, inpakken](https://en.wikipedia.org/wiki/Esperanto)
2. Kovri is een gratis, gedecentraliseerde technologie voor anonimiteit, gebaseerd op de open specificaties van [I2P](https://getmonero.org/resources/moneropedia/i2p.html)

## Disclaimer
- Kovri is op dit moment **alfa**-software die intensief wordt ontwikkeld (en nog niet is geïntegreerd met Monero).

## Snel starten

- Wil je compileerde binaire bestanden? [Download ze hieronder](#downloads)
- Wil je Kovri zelf compileren en installeren? [Zie de instructies voor compileren hieronder](#building)

## Meertalige README
Dit is een vertaling van de README van Kovri. De originele versie in het Engels is beschikbaar op: https://gitlab.com/kovri-project/kovri/blob/master/README.md

## Downloads

### Releases

Soon[tm]

### Nachtelijke releases (de allernieuwste versie)

Soon[tm]

## Testdekking

| Type      | Status |
|-----------|--------|
| Coverity  | [![Coverity Status](https://scan.coverity.com/projects/7621/badge.svg)](https://scan.coverity.com/projects/7621/)
| Coveralls | [![Coveralls Status](https://coveralls.io/repos/github/monero-project/kovri/badge.svg?branch=master)](https://coveralls.io/github/monero-project/kovri?branch=master)
| Licentie  | [![License](https://img.shields.io/badge/license-BSD3-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Compileren

### Afhankelijkheden en omgeving

| Afhankelijkheid     | Minimumversie                | Optioneel | Arch Linux  | Ubuntu/Debian    | macOS (Homebrew) | FreeBSD       | OpenBSD     |
| ------------------- | ---------------------------- |:--------:| ----------- | ---------------- | ---------------- | ------------- | ----------- |
| git                 | 1.9.1                        |          | git         | git              | git              | git           | git         |
| gcc                 | 4.9.2                        |          | gcc         | gcc              |                  |               |             |
| clang               | 3.5 (3.6 op FreeBSD)         |          | clang       | clang            | clang (Apple)    | clang36       | llvm        |
| CMake               | 3.5.1                        |          | cmake       | cmake            | cmake            | cmake         | cmake       |
| gmake (BSD)         | 4.2.1                        |          |             |                  |                  | gmake         | gmake       |
| Boost               | 1.58                         |          | boost       | libboost-all-dev | boost            | boost-libs    | boost       |
| OpenSSL             | Altijd laatste stabiele versie |          | openssl     | libssl-dev       | openssl          | openssl       | openssl     |
| Doxygen             | 1.8.6                        |    X     | doxygen     | doxygen          | doxygen          | doxygen       | doxygen     |
| Graphviz            | 2.36                         |    X     | graphviz    | graphviz         | graphviz         | graphviz      | graphviz    |
| Docker              | Altijd laatste stabiele versie |    X     | Zie site | Zie site      | Zie site      | Zie site   | Zie site |

#### Windows (MSYS2/MinGW-64)
* Download het [MSYS2-installatieprogramma](http://msys2.github.io/) dat je nodig hebt, 64-bits of 32-bits.
* Gebruik de snelkoppeling voor de architectuur van je systeem om de MSYS2-omgeving te starten. Op 64-bits systemen is dat de snelkoppeling `MinGW-w64 Win64 Shell`. Opmerking: op een 64-bits versie van Windows heb je zowel een 64-bits als een 32-bits omgeving.
* Werk de pakketten in de MSYS2-installatie bij:

```shell
$ pacman -Sy
$ pacman -Su --ignoregroup base
$ pacman -Syu
```

#### Installatiepakketten

Opmerking: Voor i686-systemen vervang je `mingw-w64-x86_64` door `mingw-w64-i686`.

`$ pacman -S make mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64-x86_64-boost mingw-w64-x86_64-openssl`

Optioneel:

`$ pacman -S mingw-w64-x86_64-doxygen mingw-w64-x86_64-graphviz`

### Maken en installeren

**Gebruik *niet* het zip-bestand van GitHub, maar voer een recursieve kloon uit.**

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
$ cd kovri && make release  # zie de Makefile voor alle compileeropties
$ make install
```

- Eindgebruikers MOETEN `make install` uitvoeren voor een nieuwe installatie.
- Ontwikkelaars MOETEN `make install` uitvoeren na een nieuwe build.

### Docker

Of compileer lokaal met Docker:

```bash
$ docker build -t kovri:latest .
```

## Documentatie en ontwikkeling
- Er is een [Gebruikershandleiding](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/nl/user_guide.md) voor alle gebruikers.
- Er is een [Handleiding voor ontwikkelaars](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/nl/developer_guide.md). Lees deze handleiding voordat je een pull request opent.
- Meer documentatie vind je in de taal van je keuze in de repository [kovri-docs](https://gitlab.com/kovri-project/kovri-docs/).
- We raden alle gebruikers en ontwikkelaars aan de [Moneropedia](https://getmonero.org/resources/moneropedia/kovri.html) te lezen.
- Via het [Forum Funding System](https://forum.getmonero.org/8/funding-required) kun je betaald worden voor je werk. [Lees hier hoe je een voorstel indient](https://forum.getmonero.org/7/open-tasks/2379/forum-funding-system-ffs-sticky).
- [build.getmonero.org](https://build.getmonero.org/) of monero-build.i2p voor gedetailleerde informatie over builds.
- [repo.getmonero.org](https://repo.getmonero.org/monero-project/kovri) of monero-repo.i2p zijn alternatieven voor GitHub met passieve toegang tot de repository.
- Zie ook [kovri-site](https://gitlab.com/kovri-project/kovri-site) en [monero/kovri meta](https://github.com/monero-project/meta).

## Reactie op beveiligingsproblemen
- Ons [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) is bedoeld om verantwoordelijke bekendmaking te stimuleren.
- We zijn ook bereikbaar via [HackerOne](https://hackerone.com/monero).

## Contact en ondersteuning
- IRC: [Freenode](https://webchat.freenode.net/) | Irc2P met Kovri
  - `#kovri` | Kanaal voor community en ondersteuning
  - `#kovri-dev` | Kanaal voor ontwikkelaars
- [Monero Mattermost](https://mattermost.getmonero.org/)
- [Monero Slack](https://monero.slack.com/) (vraag een uitnodiging op IRC)
- [Monero StackExchange](https://monero.stackexchange.com/)
- [Reddit /r/Kovri](https://www.reddit.com/r/Kovri/)
- Twitter: [@getkovri](https://twitter.com/getkovri)
- E-mail:
  - Algemeen/mediacontacten
    - dev [at] getmonero.org
  - Alle overige contacten
    - anonimal [at] kovri.io
    - Vingerafdruk van PGP-sleutel: 1218 6272 CD48 E253 9E2D  D29B 66A7 6ECF 9144 09F1

## Giften
- Bezoek onze pagina [Giften](https://getmonero.org/getting-started/donate/) om geld te geven aan Kovri.
