[<img width="300" src="https://static.getmonero.org/images/kovri/logo.png" alt="ˈKoʊvriː" />](https://gitlab.com/kovri-project/kovri)

1. [Cubrir, Tapar, Envolver](https://en.wikipedia.org/wiki/Esperanto)
2. Una tecnología  libre, descentralizada y anónima basada en especificaciones de  [I2P](https://getmonero.org/resources/moneropedia/i2p.html)

## Exclusión de garantías y limitación de responsabilidad
- Software actualmente en **pre-alfa**; bajo desarrollo extensivo (y aun no integrado con Monero)

## Guía Rápida

- Quieres binarios pre-compilados? [Descarga abajo](#Descargas)
- Quieres compilar e instalar tú mismo? [Instrucciones de compilado abajo](#Compilando)

## Léeme multi-lenguaje
Esta es una versión del archivo Léeme de Kovri, traducido al español, el original en Ingles está disponible en: https://gitlab.com/kovri-project/kovri/blob/master/README.md

## Descargas

### Lanzamientos

Soon[tm]

### Lanzamientos Nocturnos (la más reciente)

Soon[tm]

## Coverage

| Tipo      | Estado |
|-----------|--------|
| Coverity  | [![Estado de Coverity](https://scan.coverity.com/projects/7621/badge.svg)](https://scan.coverity.com/projects/7621/)
| Coveralls | [![Estado de Coveralls](https://coveralls.io/repos/github/monero-project/kovri/badge.svg?branch=master)](https://coveralls.io/github/monero-project/kovri?branch=master)
| Licencia   | [![Licencia](https://img.shields.io/badge/license-BSD3-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Compilando

### Dependencias y entorno

| Dependencia          | Versiones mínimas           | Opcional | Arch Linux  | Ubuntu/Debian    | macOS (Homebrew) | FreeBSD       | OpenBSD     |
| ------------------- | ---------------------------- |:--------:| ----------- | ---------------- | ---------------- | ------------- | ----------- |
| git                 | 1.9.1                        |          | git         | git              | git              | git           | git         |
| gcc                 | 4.9.2                        |          | gcc         | gcc              |                  |               |             |
| clang               | 3.5 (3.6 en FreeBSD)         |          | clang       | clang            | clang (Apple)    | clang36       | llvm        |
| CMake               | 3.5.1                        |          | cmake       | cmake            | cmake            | cmake         | cmake       |
| gmake (BSD)         | 4.2.1                        |          |             |                  |                  | gmake         | gmake       |
| Boost               | 1.58                         |          | boost       | libboost-all-dev | boost            | boost-libs    | boost       |
| OpenSSL             | Siempre la última versión estable |          | openssl     | libssl-dev       | openssl          | openssl       | openssl     |
| Doxygen             | 1.8.6                        |    X     | doxygen     | doxygen          | doxygen          | doxygen       | doxygen     |
| Graphviz            | 2.36                         |    X     | graphviz    | graphviz         | graphviz         | graphviz      | graphviz    |
| Docker              | Siempre la última versión estable |    X     | Ver sitio web | Ver sitio web      | Ver sitio web      | Ver sitio web   | Ver sitio web |

#### Windows (MSYS2/MinGW-64)
* Descarga el instalador de [MSYS2](http://msys2.github.io/), 64-bit o 32-bit
* Usa el acceso directo asociado con tu arquitectura, para abrir el entorno MSYS2. En sistemas 64-bit  seria `MinGW-w64 Win64 Shell`. Nota: si estas en 64-bit Windows, vas a necesitar ambos entornos 64-bit y 32-bit
* Actualiza los paquetes en tu instalación de MSYS2:

```shell
$ pacman -Sy
$ pacman -Su --ignoregroup base
$ pacman -Syu
```

#### Instala los paquetes

Nota: Para instalaciones i686, reemplaza `mingw-w64-x86_64` con `mingw-w64-i686`

`$ pacman -S make mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64-x86_64-boost mingw-w64-x86_64-openssl`

Opcional:

`$ pacman -S mingw-w64-x86_64-doxygen mingw-w64-x86_64-graphviz`

### Make e install

***No* uses el archivo zip de github: haz un clonado recursivo**

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
$ cd kovri && make release # lee el archivo Makefile para más opciones
$ make install
```

- Usuarios finales DEBEN correr el comando `make install` para instalaciones nuevas
- Desarrolladores DEBERÍAN correr `make install` después de cada compilado

### Docker

O compila localmente con Docker

```bash
$ docker build -t kovri:latest .
```

## Documentación y desarrollo
- Una [Guía de usuario](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/user_guide.md) esta disponible para todos los usuarios
- Una [Guía de desarrolladores](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/developer_guide.md) esta disponible para los desarrolladores (por favor, leer antes de abrir un nuevo pull request)
- Hay más documentación en tu idioma en el repositorio de [kovri-docs](https://gitlab.com/kovri-project/kovri-docs/)
- [Moneropedia](https://getmonero.org/es/resources/moneropedia/kovri.html) es recomendado para todos los usuarios y desarrolladores
- [Forum Funding System](https://forum.getmonero.org/8/funding-required) para obtener fondos para tu trabajo, [envía una propuesta](https://forum.getmonero.org/7/open-tasks/2379/forum-funding-system-ffs-sticky)
- [build.getmonero.org](https://build.getmonero.org/) o monero-build.i2p para información detallada de los archivos compilados
- [repo.getmonero.org](https://repo.getmonero.org/monero-project/kovri) o monero-repo.i2p son alternativas a github para repositorios de acceso sin push
- Ve tambien [kovri-site](https://gitlab.com/kovri-project/kovri-site) y [monero/kovri meta](https://github.com/monero-project/meta)

## Respuesta vulnerabilidades
- Nuestro [Proceso de respuesta de Vulnerabilidades](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) alienta la divulgación responsable
- También estamos en [HackerOne](https://hackerone.com/monero)

## Contacto y Soporte
- IRC: [Freenode](https://webchat.freenode.net/) | Irc2P con Kovri
  - `#kovri` | Canal de la comunidad y soporte
  - `#kovri-dev` | Canal de desarrollo
- [Monero Mattermost](https://mattermost.getmonero.org/)
- [Monero Slack](https://monero.slack.com/) (pide una invitación en el IRC)
- [Monero StackExchange](https://monero.stackexchange.com/)
- [Reddit /r/Kovri](https://www.reddit.com/r/Kovri/)
- Twitter: [@getkovri](https://twitter.com/getkovri)
- Email:
  - Propósito general / contacto con los medios
    - dev [at] getmonero.org
  - Otros contactos
    - anonimal [at] kovri.io
    - Huella de PGP: 1218 6272 CD48 E253 9E2D  D29B 66A7 6ECF 9144 09F1

## Donaciones
- Visita nuestra [Pagina de Donaciones](https://getmonero.org/getting-started/donate/) para ayudar a Kovri con tus donaciones
