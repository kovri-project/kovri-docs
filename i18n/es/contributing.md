## Seguros de calidad
- Mira nuestros [Seguros de calidad](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/es/quality.md) para tener una idea de nuestro flujo de trabajo

## Conformidad
- Nosotros apuntamos a usar las conformidades de C++11/14; siéntete libre de usar esto para mejorar tu trabajo
- También es altamente recomendable usar las librerías y dependencias estándar cuando sea posible

## Enviando tú trabajo
Para contribuir tu trabajo, por favor procede con lo siguiente:

1. Forkea Kovri
2. Lee nuestra [guía de estilo](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/es/style.md)
3. Crea una [branch](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) para trabajar
4. [**Firma**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) tu(s) commit(s)
5. envía tu pull-request a la branch ```master```
   - Actualmente no tenemos ningún tag por estar en Alpha. Por ahora, puedes basar tu trabajo desde la rama master.
   - Los mensajes del Commit deben ser verbose por default, considerando una línea de asunto de (50 caracteres máximo), una línea en blanco y una explicación detallada como párrafo(s) separado(s) - a menos que el titulo se auto-explique.
   - El título de commit debe preponer class u otro aspecto del proyecto. Por ejemplo, "HTTPProxy: implement User-Agent scrubber. Fixes #193." o "Garlic: fix uninitialized padding in ElGamalBlock".
   - Si un commit en particular referencia otro issue, por favor agrega una referencia. Por ejemplo, "See #123", o "Fixes #123". Esto ayudara a resolver tickets cuando se mezclen con ```master```.
   - En general, los commits deben ser [atomicos](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) y los diffs deben ser fáciles de leer. Por esta razón, por favor intenta no mezclar arreglos de formato con commits de sin formato.
   - El cuerpo de los pull requests deben contener una descripción adecuada de lo que hace el parche, y proveer una justificación o razonamiento para el parche (cuando sea apropiado). Debes incluir referencias a cualquier discusión como tickets, chats, o IRC.

## Propuestas
Para contribuir una propuesta, por favor lee nuestros [issues abiertos](https://gitlab.com/kovri-project/kovri/issues) para ver los ya existentes. Si lo que quieres proponer no está ahí, entonces [abre un nuevo issue](https://gitlab.com/kovri-project/kovri/issues/new).

que nosotros mezclamos todo, pedimos abrir una propuesta por las siguientes razones:

1. Una propuesta abre la comunicación
2. Una propuesta muestra que el colaborador respeta la opinión de todos los colaboradores del proyecto
3. Una propuesta permite a los colaboradores opinar en un foro abierto
4. Una propuesta ahorra tiempo si un colaborador ya está trabajando en algo similar
5. Una propuesta previene catástrofes, y errores, o permite a los colaboradores prepararse para catástrofes, o errores

*No* abrir una nueva propuesta *no* va a prevenirte de colaborar; nosotros mezclaremos tu PR - pero una propuesta es altamente recomendada.

## TODO
- Haz una búsqueda rápida en el código base por ```TODO(unassigned):``` y/o elige un ticket y ¡comienza a trabajar!
- Si creas un TODO, asígnatelo a ti mismo o escribe ```TODO(unassigned):```
