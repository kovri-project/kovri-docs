# تشغيل مونيرو علي كوفري

## البدأ

1. اتبع التعليمات لبناء او تنصيب [كوفري](https://gitlab.com/kovri-project/kovri) & [مونيرو](https://github.com/monero-project/monero)
2. راجع [دليل المستخدم](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/user_guide.md) باللغه التي تفضلها
3. جهّز انفاق سيرفر وخادم كوفري للإتصال بخادم مونيرو من خلال شبكه I2P

## نفق خادم كوفري

في البدايه جهّز نفق خادم علي عقده واحده.

لإعداد نفق خادم شاهد `tunnels.conf` بداخل مجلد البيانات. لمعرفه مسار مجلد البيانات شاهد`kovri.conf`. 

```
[XMR_P2P_Server]
type = server
address = 127.0.0.1
port = 28080
in_port = 28080
keys = xmr-p2p-keys.dat
;white_list =
;black_list =
```

ستقوم هذه الإعدادات بفتح مستمع كوفري على منفذ P2P الافتراضي لـ monerod. بعد ذلك ، اقرأ [دليل المستخدم] (https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/ar/user_guide.md) لمعرفة كيفية الحصول على عنوان الوجهة الأساسي 32.

إذا كنت قد بدأت بالفعل كوفري فقم بتشغيل `$ kill -HUP $(pgrep kovri)` لتحميل النفق الجديد. يمكنك أيضا القيام بعملية إعادة تشغيل عن طريق إيقاف وبدء كوفري.

## نفق عميل كوفري

والآن بعد أن تم إعداد نفق الخادم ، ابدأ إعداد نفق العميل من خلال الإشارة إلى نفق الخادم.

مثال:

```
[XMR_Client]
type = client
address = 127.0.0.1
port = 24040
dest = your-copied-server-destination.b32.i2p
dest_port = 28080
keys = xmr-client-keys.dat
```

كرر العملية لكل عقدة ترغب في الاتصال بها باستخدام كوفري.

إذا كنت قد بدأت بالفعل كوفري فقم بتشغيل `$ kill -HUP $(pgrep kovri)` لتحميل النفق الجديد. يمكنك أيضا القيام بعملية إعادة تشغيل عن طريق إيقاف وبدء كوفري.

## مونيرو P2P من خلال كوفري

توجيه `monerod` إلى نفق عميل كوفري الجديد هو بنفس السهولة.

هنا مثال على بدء عقدة الشبكه التجريبيه للتعدين:

```bash
monerod --testnet --start-mining your-testnet-wallet-address --add-exclusive-node 127.0.0.1:24040
```

إذا لاحظت مشكلات في الاتصال ، فانتظر حتى يتم دمج خادم كوفري في الشبكة (~ 5-10 دقائق بعد البدء).

## مونيرو RPC من خلال كوفري

تشغيل خدمة RPC من مونيرو عبر كوفري هي عملية مماثلة.

تكوين نفق خادم على عقدة كوفري الخاصة بك:

```
[XMR_RPC_Server]
type = server
address = 127.0.0.1
port = 28081
in_port = 28081
keys = xmr-rpc-keys.dat
;white_list =
;black_list =
```

سيؤدي ذلك إلى إنشاء مجموعة جديدة من المفاتيح وعنوانًا جديدًا للمقصد.

لاستخدام نفس عنوان الوجهة P2P ، ما عليك سوى استبدال `xmr-rpc-keys.dat` بـ` xmr-p2p-keys.dat` في التكوين أعلاه.

إذا كنت قد بدأت بالفعل كوفري فقم بتشغيل `$ kill -HUP $(pgrep kovri)` لتحميل النفق الجديد. يمكنك أيضا القيام بعملية إعادة تشغيل عن طريق إيقاف وبدء كوفري.

يعرض هذا النفق شبكه الإختبار "monerod` الافتراضي" JSON RPC ، ولا يمكن لأحد استخدام نفق عميل HTTP كوفري للاتصال.

في ما يلي مثال للتواصل مع curl من عقدة عميل كوفري:

```bash
export http_proxy=127.0.0.1:4446 rpc_server=http://your-copied-rpc-destination.b32.i2p:28081
curl -X POST ${rpc_server}/json_rpc -d '{"jsonrpc":"2.0","id":"0","method":"get_height"}' -H 'Content-Type: application/json'
```
