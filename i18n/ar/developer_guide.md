# القواعد الارشادية
- نهدف إلى التوافق التام مع C ++ 11/14 ؛ يرجى استخدام هذا لصالحك
- الرجاء استخدام المكتبة القياسية ومكتبات التبعية كلما أمكن ذلك

## استجابة الضعف
- تشجع [عملية الاستجابة للثغرات] (https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) على الإفصاح المسؤول
- نحن أيضًا متاحون عبر [HackerOne] (https://hackerone.com/monero)

## نمط
1. إقرأ [دليل تنسيق جوجل لـ C++](https://google.github.io/styleguide/cppguide.html) (خاصةً لمرجع نمط غير التنسيق)
   - لبرمجه bash , إقرأ [دليل تنسيق جوجل لـ shell](https://github.com/google/styleguide/blob/gh-pages/shell.xml)
2. للملفات التي تحتوي فقط علي عمل جديد [clang-format](http://clang.llvm.org/docs/ClangFormat.html) with ```-style=file``` (الذي يستخدم تنسيقنا [.clang-format](https://gitlab.com/kovri-project/kovri/blob/master/.clang-format))
```bash
$ cd kovri/ && clang-format -i -style=file src/path/to/my/file
```
3. بالنسبة للملفات ذات الأعمال المختلطة (الموجودة + الجديدة) ، قم بتشغيل [clang-format] (http://clang.llvm.org/docs/ClangFormat.html) بشكل انتقائي على الأسطر المرتبطة مباشرة بالعمل الجديد.
   - راجع [vim] (http://clang.llvm.org/docs/ClangFormat.html#vim-integration) و [emacs] (http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) وثائق للحصول على أمثلة لتكوين keybindings لمكونات clang-format`.
4. تشغيل [cpplint] (https://github.com/google/styleguide/tree/gh-pages/cpplint) (الذي يستخدم موقعنا المقدم [CPPLINT.cfg] (https://gitlab.com/kovri-project/kovri/ blob / master / CPPLINT.cfg)) للقبض على أية مشكلات تم تفويتها بواسطة تنسيق clang
```bash
$ cd kovri/ && cpplint src/path/to/my/file && [edit file manually to apply fixes]
```

### الإضافات

- Vim دمج
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#vim-integration)
  - [clang-format ubuntu 16.04 vim workaround](http://stackoverflow.com/questions/39490082/clang-format-not-working-under-gvim)
  - [cpplint.vim](https://github.com/vim-syntastic/syntastic/blob/master/syntax_checkers/cpp/cpplint.vim)
- Emacs دمج
  - [clang-format](http://clang.llvm.org/docs/ClangFormat.html#emacs-integration) + [clang-format.el](https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.el)
  - [flycheck-google-cpplint.el](https://github.com/flycheck/flycheck-google-cpplint)

### تعديلات على نمط C ++ المقترح من جوجل

- تجنب الحالة المختلطة prepended `` `k``` و MACRO_TYPE لجميع الثوابت
- استخدم Doxygen three-slash `` `// C ++ comments``` عند توثيق Doxygen
- حاول توثيق كل عملك من أجل Doxygen أثناء تقدمك
- إذا كان عدم الكشف عن هويتك مصدر قلق ، فحاول مزجها مع أسلوب المساهم الحالي

### فحوصات إختياريه
1. [cppdep](https://github.com/rakhimov/cppdep)
   للاعتمادية المكون والعزل المادي ، وتشمل الفحوصات.
2. [cppcheck](https://github.com/danmar/cppcheck/) لتحليل ثابت
   (مكمل للغطاء).
3. [lizard](https://github.com/terryyin/lizard) للتحقق من تعقيدات الكود.

## إرسال عملك
للمساهمة يرجى متابعة ما يلي:

1. [إنشقاق](https://help.github.com/articles/fork-a-repo/) كوفري
2. راجع قسم التنسيق في هذه الصفحه
3. إنشاء [فرع موضوع](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
   - ليس لدينا حاليًا أي علامات فنحن في مرحله ألفا. في الوقت الحالي ، يمكنك بناء عملك خارج الموقع الرئيسي
4. احدث تغيير
   - يجب أن يكون الالتزام [ذريًا] (https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) عندما يكون ذلك ممكنًا وأن يكون الاختلاف سهل القراءة
   - الرجاء محاولة عدم خلط إصلاحات التنسيق مع عمليات الالتزام غير المنسقة
5. كن مهذب مع تسجيلات git
   - يجب أن يحمل عنوان الالتزام فئة أو جانبًا من المشروع. علي سبيل المثال, "HTTPProxy: تنظيف تطبيق عميل المستخدم. اصلاح #193." or "Garlic: إصلاح الحشو غير المصنف في ElGamalBlock"
   - يجب أن تكون رسائل الالتزام مطبقة افتراضياً ، وتتألف من سطر قصير (50 حرفاً كحد أقصى) ، وسطر فارغ ، ونص تفسيري تفصيلي كفقرة (فقرات) منفصلة. - ما لم يكن العنوان وحده لا يحتاج إلى شرح
   - إذا أشار التزام معين إلى مشكلة أخرى ، فيرجى إضافة مرجع. فمثلا؛ * راجع # 123 * أو * اصلاح # 123 *. سيساعدنا ذلك في حل المشكلات عند الدمج في الرئيسي
   - إذا تمت إعادة رسم التزام معين بعد التعاون ضمن طلب سحب ، فالرجاء الإشارة إلى رقم طلب السحب داخل رسالة الالتزام. فمثلا؛ * المراجع # 123 *
6. [** وقّع **] (https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) التزاماتك وإذا كنت مساهماً جديداً ، افتح طلب سحب جديد يضيف مفتاح PGP الخاص بك إلى مستودعنا (انظر المساهمة)
7. أرسل طلب سحب إلى الفرع الرئيسي
   - يجب أن يحتوي نص طلب السحب على وصف دقيق لما يفعله التصحيح ويجب أن يوفر أيضًا مبررًا / منطقًا للرزمة (عند الاقتضاء). يجب عليك تضمين إشارات إلى أي مناقشات مثل القضايا الأخرى أو الدردشات على IRC

## الإقتراحات
للمساهمة في اقتراح ، يرجى مراجعة [القضايا المفتوحة] (https://gitlab.com/kovri-project/kovri/issues) الخاصة بالمقترحات الحالية. إذا لم يكن ما تقترحه موجودًا ، فعندئذ [افتح مشكلة جديدة] (https://gitlab.com/kovri-project/kovri/issues/new).

نطلب منك فتح اقتراح للأسباب التالية:

1. الإقتراح يتيح التواصل
2. يُظهر الإقتراح أن المساهم يحترم مدخلات جميع المتعاونين في المشروع
3. يسمح الاقتراح بإدخال متعاون سلس في منتدى مفتوح
4. يوفر الاقتراح الوقت إذا كان أحد المتعاونين يعمل على ميزة / مشكلة مشابهة
5. اقتراح يمنع الكوارث والحوادث أو يسمح للمتعاونين للتحضير للكوارث والحوادث

*عدم* فتح إقتراح لن يمنعك من المساهمه, سوف ندمج طلب السحب الخاص بك - لكن فتح إقتراح أمر موصي به.

## تحتاج للتطوير
- ابحث في الكود عن  ```TODO(unassigned):``` واختار مشكله وإبدأ بمعالجتها!
- إذا قمت بإنشاء مشكله عيّنها لنفسك أو إكتب ```TODO(unassigned):```

## Unit-test writing

Test writing is a well-trodden path whose process should not come as a surprise (as there are many decades of tests to study in the software repertoire). For this project, we will focus on the following when writing unit-tests as they are considered a standard *good practice*:

- Err on the side of [TDD](https://en.wikipedia.org/wiki/Test-driven_development) (refactor when necessary)
- Focus on modular programming / separation of concerns
- Test the quality of code coverage, not simply quantity
- Avoid running the same code paths across multiple tests
- Avoid copypasting implementation into test code

Also note that the state of the data - *not the context of the state* - should be held paramount as a driver for unit TDD.

Now, while there are many good, working examples of how to write unit-tests, let's look at some popular and recommended idioms as presented by our cousin [Tor](https://gitweb.torproject.org/tor.git/tree/doc/HACKING/WritingTests.md):

>If your code is very-low level, and its behavior is easily described in
terms of a relation between inputs and outputs, or a set of state
transitions, then it's a natural fit for unit tests.  (If not, please
consider refactoring it until most of it _is_ a good fit for unit
tests!)

>If your code adds new externally visible functionality to Tor, it would
be great to have a test for that functionality.  That's where
integration tests more usually come in.

>When writing tests, it's not enough to just generate coverage on all the
lines of the code that you're testing:  It's important to make sure that
the test _really tests_ the code.

>Remember, the purpose of a test is to succeed if the code does what
it's supposed to do, and fail otherwise.  Try to design your tests so
that they check for the code's intended and documented functionality
as much as possible.

>Often we want to test that a function works right, but the function to
be tested depends on other functions whose behavior is hard to observe, or
which require a working Tor network, or something like that.

>We talk above about "test coverage" -- making sure that your tests visit
every line of code, or every branch of code.  But visiting the code isn't
enough: we want to verify that it's correct.

>So when writing tests, try to make tests that should pass with any correct
implementation of the code, and that should fail if the code doesn't do what
it's supposed to do.

>You can write "black-box" tests or "glass-box" tests.  A black-box test is
one that you write without looking at the structure of the function.  A
glass-box one is one you implement while looking at how the function is
implemented.

>In either case, make sure to consider common cases *and* edge cases; success
cases and failure csaes.

>Tests shouldn't require a network connection.

>When possible, tests should not be over-fit to the implementation.  That is,
the test should verify that the documented behavior is implemented, but
should not break if other permissible behavior is later implemented.

In addition to not requiring a network connection, *unit-tests* should not require socket or filesystem access unless the test is socket/filesystem-specific test (these are unit-tests, not inteegration tests).

Other notes:

- Though we have a Docker testnet, we currenly lack any effective framework for integration and system testing. As such, the best we can do at the moment is effective unit testing.
- For gcov output when building tests, build with `make coverage`. This target should also build unit-tests.
- For existing kovri examples, see `crypto/{ed25519.cc,radix.cc}` and `util/buffer.cc` to name a few
- For effective unit-test writing outside of Tor, see Crypto++ and Monero unit-tests

## اختبار الزغب

من [المرجع] (http://llvm.org/docs/LibFuzzer.html): "LibFuzzer قيد التطوير النشط لذا ستحتاج إلى النسخة الحالية (أو على الأقل حديثة جدًا) من مترجم Clang"

احصل على نسخة حديثة من clang:

```bash
$ cd ~/ && mkdir TMP_CLANG && git clone https://chromium.googlesource.com/chromium/src/tools/clang TMP_CLANG/clang
$ ./TMP_CLANG/clang/scripts/update.py
$ cd --
```

احصل علي libFuzzer:

```bash
$ git clone https://chromium.googlesource.com/chromium/llvm-project/llvm/lib/Fuzzer contrib/Fuzzer
```

إبني كوفري مع السماح بإختبار fuzz:

```bash
$ PATH="~/third_party/llvm-build/Release+Asserts/bin:$PATH" CC=clang CXX=clang++ make fuzz-tests
```

الإستخدام (مثال لـ RouterInfo):

```bash
mkdir RI_CORPUS MIN_RI_CORPUS
find ~/.kovri/core/network_database/ -name "router_info*" -exec cp {} RI_CORPUS \;
./build/kovri-util fuzz --target=routerinfo -merge=1 MIN_RI_CORPUS RI_CORPUS
./build/kovri-util fuzz --target=routerinfo -jobs=2 -workers=2 MIN_RI_CORPUS
```

# تاكيد الجودة

فيما يلي نموذج مقترح لسير عمل ضمان الجودة. في حين أنها خطية بطبيعتها ، يمكن العمل على أي مرحلة على حدة إذا لزم الأمر - طالما يتم معالجة جميع المراحل في نهاية المطاف.

## المرحلة 1: المراجعة الأساسية

- مراجعة المشكلات المفتوحة في [المتعقّب](https://gitlab.com/kovri-project/kovri/issues/)
- مراجعة [عملية الاستجابة للثغرات](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md)
- يجب أن تلتزم جميع التعليمات البرمجية بإرشادات المساهمة
- ملاحظة المجالات التي تحتاج إلى تحسين (ذهنيا أو في التعليمات البرمجية)
- - إنشاء TODO وتعيين إذا كان ذلك ممكنا

## المرحلة الثانية: مراجعة المواصفات / التنفيذ / توثيق الكود

- مراجعة المواصفات كاملة على أساس كل وحدة نمطية ؛ على سبيل المثال ، العرض ، I2PControl ، إلخ.
   - يجب أن يكون الكود متوافقًا مع الأجزاء الأساسية من المواصفات التي ستحافظ على نفس مستوى (أو أفضل) من إخفاء الهوية الذي توفره جافا I2P
   اعاده التكويد / تنفيذ / التصحيح عندما / عند الحاجة
- ضمان التنفيذ المتوافق C ++ 11/14
   - مراجعة المرحلة 2 إذا لزم الأمر
- حل جميع TODO ذات الصلة
- رمز المستند قدر الإمكان مع التعليقات المضمنة و Doxygen
   - ينبغي أن يفهم المدونة من قبل المبتدئ إلى المبرمجين ذوي الخبرة
   - يجب على الكود توجيه القارئ إلى فهم أفضل لـ I2P
     - I2P معقد للغاية لذا يجب أن يكون كودنا بمثابة استبدال سيادي لوثائق المواصفات وليس مجرد ملحق (يمكن أن يكون هذا هدفاً شاقًا ولكنه مفيد للغاية من حيث الصيانة وعمر البرمجيات)

## المرحلة الثالثة: مراجعة التشفير / التدقيق الأمني

- التأكد من أن التشفير جاهز ويتم تنفيذه بشكل صحيح
- إنشاء كل ناقل لاستغلال معروف
   - ضع هذه المتجهات في الاعتبار عند كتابة الاختبارات
- كسر كوفري في كل اتجاه ممكن
   - اصلاح ما كسر
- استخدم دائمًا مكتبات موثوق بها ومكتوبه بعنايه عندما يكون ذلك ممكنًا
   - تجنّب Homebrewed ، ad-hoc ، نوع الكود من * أنا متأكد أنني أعرف أفضل من المجتمع *
- اطلب رأي (أو أكثر) من الزملاء قبل الانتقال إلى المرحلة التالية

## المرحلة 4: سحق الأخطاء / اختبارات / التنميط

- حل الأخطاء / القضايا ذات الأولوية
- كتابة اختبارات لكل وحدة
   - إبدأ الاختبارات. تشغيلها مرة أخرى
   - مراجعة كاملة لنتائج الاختبار. التصحيح إذا لزم الأمر. اعاده الكتابه حسب الضرورة
- تأكد من أن التشغيل التلقائي يعمل بانتظام
   - valgrind ، doxygen ، clang-format
   - تصحيح إذا لزم الأمر ، اعاده الكتابه حسب الضرورة

## المرحلة الخامسة: التشاور

- التشاور مع الزملاء والمجتمع
   - يجب أن يتم التقديم بشكل عام عن طريق القضايا والاجتماعات و / أو IRC
- قبول جميع ردود الفعل ، وردا على ذلك ، تقديم نتائج ملموسة
- إذا كنت راضيًا ، تابع المرحلة التالية ، أو كرر هذه المرحلة (أو ابدأ من مرحلة سابقة)

## المرحلة 6: كرر الدورة من البداية
