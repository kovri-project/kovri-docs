## الشكر والتقدير
بالإضافه إلي جميع مطورين Boost و OpenSSL, وجميع المبرمجين منذ time-immemorial الذي جعل هذا المشروع ممكناً نود بالإقرار بالشكر والتقدير لكل من:

- Wei Dai و Jeffrey Walton وكل المطورين في Crypto++ لتوفير مكتبه تشفير مؤثره ومجانيه 
- Dean Michael Berris, Glyn Matthews,وجميع المطورين في cpp-netlib لتوفير مكتبة الشبكات للأنظمه المختلفه المطوره بجِد
- zzz, str4d, and جميع مطورين جافا وبروتوكول الإنترنت الخفي في السابق وحالياً الذين وضعوا معيار لجميع تطبيقات I2P الأخرى لمتابعة 
- orion لتوفير ```i2pcpp```: the [original](http://git.repo.i2p.xyz/w/i2pcpp.git) تنفيذ C ++ من I2P
- orignal لتوفير ```i2pd```: تنفيذ C ++ الأكثر اكتمالا من I2P ل [us to fork from](https://github.com/purplei2p/i2pd/commit/45d27f8ddc43e220a9eea42de41cb67d5627a7d3)
- EinMByte لتحسين جميع تطبيقات C ++ I2P (بما في ذلك كوفري)
