# Cheatsheet - quick reference for various specs

## RouterInfo Sizes

Maximum sizes are the maximum allowed by the size byte(s) data type, unless otherwise noted in the specification.

### Certificate:

* `1`: Type (`0`: Null, `5`: Key)
  - all others unused
* `2`: Length (`uint16_t`) (`0`: Null, `4`: Signing + Crypto key type, other fields unused)
* `2`: Signing public key type (`uint16_t`) (`0`: DSA_SHA1, `7`: EdDSA_SHA512_Ed25519)
  - all other invalid for RI
* `2`: Crypto public key type (`uint16_t`) (`0`: ElGamal)
* **Total**: `3 - 7`
  - reject all other sizes & excess data (spec recommendation)
  - [https://geti2p.net/spec/common-structures#type-certificate](https://geti2p.net/spec/common-structures#type-certificate)

### RouterIdentity:

* `256`: Public crypto key (ElGamal)
  - all other types experimental or unused
* `128`: Public signing key (EdDSA or DSA)
  - padded up to `128` if EdDSA (`32` key + `96` padding)
* `3 - 7`: Certificate (Null or Key)
* **Total**: `387 - 391`
  - [https://geti2p.net/spec/common-structures#struct-routeridentity](https://geti2p.net/spec/common-structures#struct-routeridentity)

### Mapping:

* `2`: Length (`uint16_t`)
* `0 - 65535`: UTF-8 or Unicode `Key=Value;` pairs, see spec (I2CP vs I2NP encoding)
* **Total**: `2 - 65537`
  - must be sorted
  - dups must be rejected
  - [https://geti2p.net/spec/common-structures#type-mapping](https://geti2p.net/spec/common-structures#type-mapping)

### RouterAddress:

* `1`: Cost (`0 - 255`)
* `8`: Expiration (`uint64_t`, all zeroes)
* `1 - 256`: Transport (`uint8_t[256]`: `1` size byte followed by up-to `255` UTF-8 bytes)
* `2 - 65537`: Router Options Mapping
* **Total**: `12 - 65802`
  - [https://geti2p.net/spec/common-structures#struct-routeraddress](https://geti2p.net/spec/common-structures#struct-routeraddress)

### RouterInfo:

* `387 - 391`: RouterIdentity
* `8`: Published date (`uint64_t`: UTC seconds since epoch)
* `1`: Size (`uint8_t`: number of RouterAddresses, `0 - 255`)
* `0 - 16779510`: RouterAddresses
* `1`: Peer size (`uint8_t`: always `0`)
* `2 - 65537`: NetDb Options Mapping (caps, stats, family options, etc. see spec)
  - [https://geti2p.net/en/docs/how/network-database#routerInfo](https://geti2p.net/en/docs/how/network-database#routerInfo)
* `40 - 64`: Signature (DSA or EdDSA_SHA512_Ed25519)
* **Total**: `439 - 16845512`
  - [https://geti2p.net/spec/common-structures#struct-routerinfo](https://geti2p.net/spec/common-structures#struct-routerinfo)
