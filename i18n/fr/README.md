[<img width="300" src="https://static.getmonero.org/images/kovri/logo.png" alt="ˈKoʊvriː" />](https://gitlab.com/kovri-project/kovri)

1. [Couvrir, voiler, envelopper](https://fr.wikipedia.org/wiki/Esperanto)
2. Une technologie d'anonymisation gratuire et décentralisée basée sur les spécifications ouvertes d'[I2P](https://getmonero.org/resources/moneropedia/i2p.html)

## Disclaimer
- Actuellement en **pré-alpha**; en cours de développement intensif (et pas encore intégré à monero)

## Quickstart

- Vous voulez les binaires précompilés ? [Télécharger ci-dessous](#telechargements)
- Vous voulez compiler et installer vous-même ? [Instruction de compilation](#compilation)

## Multilingual README
Cette page est également disponible dans les langues suivantes

- [English](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/README_template.md)
- [Italiano](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/it/README.md)
- [Español](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/es/README.md)
- Pусский
- [Deutsch](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/de/README.md)
- [Dansk](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/da/README.md)
- [العربية](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/ar/README.md)

## Téléchargements

### Versions

Bientôt [tm]

### Version Nocturnes (bleeding edge)

Bientôt [tm]

## Couverture

| Type      | Etat |
|-----------|--------|
| Coverity  | [![Coverity Status](https://scan.coverity.com/projects/7621/badge.svg)](https://scan.coverity.com/projects/7621/)
| Codecov   | [![Codecov](https://codecov.io/gl/kovri-project/kovri/branch/master/graph/badge.svg)](https://codecov.io/gl/kovri-project/kovri)
| License   | [![License](https://img.shields.io/badge/license-BSD3-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Compilation

### Dépendances et environnements

| Dépendances         | Version minimale                    | Optionnelle | Arch Linux       | Ubuntu/Debian    | macOS (Homebrew) | FreeBSD          | OpenBSD          |
| ------------------- | ----------------------------------- |:-----------:| ---------------- | ---------------- | ---------------- | ---------------- | ---------------- |
| git                 | 1.9.1                               |             | git              | git              | git              | git              | git              |
| gcc                 | 4.9.2                               |             | gcc              | gcc              |                  |                  |                  |
| clang               | 3.5 (3.6 on FreeBSD)                |             | clang            | clang            | clang (Apple)    | clang36          | llvm             |
| CMake               | 3.5.1                               |             | cmake            | cmake            | cmake            | cmake            | cmake            |
| gmake (BSD)         | 4.2.1                               |             |                  |                  |                  | gmake            | gmake            |
| Boost               | 1.58                                |             | boost            | libboost-all-dev | boost            | boost-libs       | boost            |
| OpenSSL             | Toujours la dernière version stable |             | openssl          | libssl-dev       | openssl          | openssl          | openssl          |
| Doxygen             | 1.8.6                               |      X      | doxygen          | doxygen          | doxygen          | doxygen          | doxygen          |
| Graphviz            | 2.36                                |      X      | graphviz         | graphviz         | graphviz         | graphviz         | graphviz         |
| Docker              | Toujours la dernière version stable |      X      | voir le site web | voir le site web | voir le site web | voir le site web | voir le site web |

#### Windows (MSYS2/MinGW-64)
* Téléchargez l'[installateur MSYS2](http://msys2.github.io/), 64-bit ou 32-bit selon votre besoin
* Utilisez le raccourci associé à votre architecture pour lancer l'environnement MSYS2. Sur un système 64 bits il s'agira du raccourci `MinGW-w64 Win64 Shell`. Remarque: si vous avez un Windows 64 bits, vous disposerez des deux environnements 32 et 64 bits
* Mettez à jour les packages dans votre installation MSYS2 :

```shell
$ pacman -Sy
$ pacman -Su --ignoregroup base
$ pacman -Syu
```

#### Installer les packages

Remarque : Pour les compilations i686, remplacez `mingw-w64-x86_64` par `mingw-w64-i686`

`$ pacman -S make mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64-x86_64-boost mingw-w64-x86_64-openssl`

Optionnellement :

`$ pacman -S mingw-w64-x86_64-doxygen mingw-w64-x86_64-graphviz`

### Make et install

***Ne pas* utiliser le fichier zip de gitLab : faites uniquement un clone récursif**

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
$ cd kovri && make release  # Voir le Makefile pour toutes les options de compilation
$ make install
```

- Les utilisateurs DOIVENT lancer `make install` pour les nouvelles installations
- Les développeurs DEVRAIENT lancer `make install` après compilation

### Docker

Ou compilez localement avec Docker

```bash
$ docker build -t kovri:latest .
```

## Documentation et Développement
- Un [Guide Utilisateur](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/fr/user_guide.md) est disponible pour tous les utilisateurs
- Un [Guide de développement](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/fr/developer_guide.md) est disponible pour les développeurs (veuillez le consulter avant d'ouvrir une *pull request*)
- Vous trouverez plus de documentation dans la langue de votre choix dans le dépôt [kovri-docs](https://gitlab.com/kovri-project/kovri-docs/)
- [Moneropedia](https://getmonero.org/fr/resources/moneropedia/kovri.html) est recommandé pour tous les utilisateurs et développeurs
- Le [Forum Funding System](https://forum.getmonero.org/8/funding-required) permet d'obtenir des fonds pour financer vos travaux, [soumettez une proposition](https://forum.getmonero.org/7/open-tasks/2379/forum-funding-system-ffs-sticky)
- [build.getmonero.org](https://build.getmonero.org/) ou monero-build.i2p pour des informations de compilation détaillées
- [repo.getmonero.org](https://repo.getmonero.org/monero-project/kovri) ou monero-repo.i2p sont des alternatives à GitLab pour l'accès au dépot en lecture
- Voir également [kovri-site](https://gitlab.com/kovri-project/kovri-site) et [monero/kovri meta](https://github.com/monero-project/meta)

## Réponse aux Vulnérabilités
- Notre [Processus de Réponse aux Vulnérabilités](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) encourage les divulgations responsables
- Nous sommes également disponible à travers [HackerOne](https://hackerone.com/monero)

## Contact et Support
- IRC: [Freenode](https://webchat.freenode.net/) | Irc2P avec Kovri
  - `#kovri` | Canal de Communauté et de Support
  - `#kovri-dev` | Canal de Développement
- Twitter: [@getkovri](https://twitter.com/getkovri)
- Email:
  - anonimal [at] kovri.io
  - PGP Key fingerprint: 1218 6272 CD48 E253 9E2D  D29B 66A7 6ECF 9144 09F1
- [Slack Kovri](https://kovri-project.slack.com/) (invitation nécessaire par Email ou IRC)
- [Mattermost Monero](https://mattermost.getmonero.org/)
- [Slack Monero](https://monero.slack.com/) (ask for an invite on IRC)
- [StackExchange Monero](https://monero.stackexchange.com/)
- [Reddit /r/Kovri](https://www.reddit.com/r/Kovri/)

## Donations
- Consultez notre [Page de dons](https://getmonero.org/getting-started/donate/) pour aider Kovri avec vos dons
