## Bidrag

## Kvalitets sikring
- Se vores [Kvalitets sikring](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/da/quality.md) 
Guide til at få en idé om foreslået arbejdsgang

## Overholdelse
- Vi sigter efter en komplet C++11/14 overholdelse; du velkommen til at bruge denne til din fordele med dit arbejde
- Det er også klart anbefalet at bruge standard biblioteket og afhængigheds biblioteker når muligt

## Sendelse af dit arbejde
For at bidrage med dit arbejde, venligst fortsæt med følgende:

1. Fork Kovri
2. Læs vores [style guide](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/da/style.md)
3. Opret en [topic branch](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
4. [**Signer**](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) dine commit(s)
5. Send en pull-request til branch ```master```
- På nuværende tidspunkt har vi ingen tags eftersom vi er i Alpha. For nu, kan du basere dit arbejde ud fra master.
- Commit beskeder burde være ordrige som standard, bestående af en kort emne linje (max 50 tegn), en blank linje, og detaljeret forklarende tekst som et seperat afsnit - med mindre titlen alene er forklarende nok. 
- Commit titel burde indeholde data eller et aspekt af projektet. F.eks. "HTTPProxy: implement User-Agent scrubber. Fixes #193." eller "Garlic: fix uninitialized padding in ElGamalBlock".
- Hvis en særlig commit referere til et andet problem, venligst tilføj en reference. F.eks. "See #123", eller "Fixes #123". Dette ville hjælpe os med at løse tickets når vi merger ind i  ```master```.
- Generelt burde commits være [atomic](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention) og diffs burde være nemme at læse. Prøv derfor ikke at blande formateringsrettelser med ikke-formateret commits.
- Kroppen af pull requesten burde indeholde en akkurat beskrivelse af hvad opdatering gør og giv begrundelse for denne opdatering (når passende). Du burde inkludere referencer til hvilken som helst diskussion som andre tickets eller chatte på IRC.

## Oplæg
For at bidrage med oplæg, venligst gennemgå vores [open issues](https://gitlab.com/kovri-project/kovri/issues) for eksisterende oplæg. Hvis hvad du foreslår ikke er der, så [open a new issue](https://gitlab.com/kovri-project/kovri/issues/new).

Så spørge vi om du åbner et oplæg for følgende grunde:

1. Et oplæg åbner op for kommunikation
2. Et oplæg viser at folk der bidrager respekterer input af alle projekt samarbejdspartnere
3. Et oplæg tillader sømløs samarbejdspartnere input i et åben forum
4. Et oplæg sparer tid hvis en samarbejdspartner arbejder på en lignende funktion/problem
5. Et oplæg forhindrer katastrofer og uheld eller tillader samarbejdspartnere at forberede på katastrofer eller uheld

ved *ikke* at åbne et oplæg forhindrer dig *ikke* i at bidrage; vi ville stadig merge hvad du PR - men et oplæg er højt anbefalet.

## TODO's
- Gør et hurtigt søg i kodebasen via ```TODO(unassigned):``` og/eller vælg et ticket og påbegynd patching!
